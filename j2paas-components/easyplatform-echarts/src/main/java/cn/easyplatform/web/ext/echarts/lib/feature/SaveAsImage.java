/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.feature;

import org.zkoss.util.resource.Labels;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SaveAsImage extends Restore {
    /**
     * 保存的图片格式。支持 'png' 和 'jpeg'
     */
    private String type;
    /**
     * 保存的文件名称，默认使用 title.text 作为名称
     */
    private String name;
    /**
     * 保存的图片背景色，默认使用 backgroundColor，如果backgroundColor不存在的话会取白色
     */
    private Object backgroundColor;
    /**
     * 保存为图片时忽略的组件列表，默认忽略工具栏
     */
    private Object[] excludeComponents;
    /**
     * 保存图片的分辨率比例，默认跟容器相同大小，如果需要保存更高分辨率的，可以设置为大于 1 的值，例如 2
     */
    private Integer pixelRatio;

    /**
     * 构造函数
     */
    public SaveAsImage() {
        this.show(true);
        this.title(Labels.getLabel("echarts.saveAsImage"));
        this.type("png");
    }

    public String type() {
        return type;
    }

    public SaveAsImage type(String type) {
        this.type = type;
        return this;
    }

    public String name() {
        return name;
    }

    public SaveAsImage name(String name) {
        this.name = name;
        return this;
    }

    public Object backgroundColor() {
        return backgroundColor;
    }

    public SaveAsImage backgroundColor(Object backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public Object[] excludeComponents() {
        return excludeComponents;
    }

    public SaveAsImage excludeComponents(Object... excludeComponents) {
        this.excludeComponents = excludeComponents;
        return this;
    }

    public Integer pixelRatio() {
        return pixelRatio;
    }

    public SaveAsImage pixelRatio(Integer pixelRatio) {
        this.pixelRatio = pixelRatio;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Object backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Object[] getExcludeComponents() {
        return excludeComponents;
    }

    public void setExcludeComponents(Object[] excludeComponents) {
        this.excludeComponents = excludeComponents;
    }

    public Integer getPixelRatio() {
        return pixelRatio;
    }

    public void setPixelRatio(Integer pixelRatio) {
        this.pixelRatio = pixelRatio;
    }
}
