/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.axis;

import cn.easyplatform.web.ext.echarts.lib.Tooltip;
import cn.easyplatform.web.ext.echarts.lib.type.AxisType;

/**
 * Created by shiny on 2017-03-09.
 */
public class SingleAxis extends BaseAxis {
    /**
     * 轴的朝向，默认水平朝向，可以设置成 'vertical' 垂直朝向
     */
    private String orient;
    /**
     * treemap 组件离容器左侧的距离
     */
    private Object left;
    /**
     * treemap 组件离容器上侧的距离
     */
    private Object top;
    /**
     * treemap 组件离容器右侧的距离
     */
    private Object right;
    /**
     * treemap 组件离容器下侧的距离
     */
    private Object bottom;

    private Object width;

    private Object height;

    private Tooltip tooltip;

    private MinorTick minorTick;

    private MinorSplitLine minorSplitLine;

    /**
     * 构造函数
     */
    public SingleAxis() {
        this.type(AxisType.value.toString());
    }

    public MinorSplitLine minorSplitLine() {
        if (minorSplitLine == null)
            minorSplitLine = new MinorSplitLine();
        return this.minorSplitLine;
    }

    public SingleAxis minorSplitLine(MinorSplitLine minorSplitLine) {
        this.minorSplitLine = minorSplitLine;
        return this;
    }


    public MinorTick minorTick() {
        if (minorTick == null)
            minorTick = new MinorTick();
        return this.minorTick;
    }

    public SingleAxis minorTick(MinorTick minorTick) {
        this.minorTick = minorTick;
        return this;
    }

    public Tooltip tooltip() {
        if (tooltip == null)
            tooltip = new Tooltip();
        return this.tooltip;
    }

    public SingleAxis tooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
        return this;
    }

    public Object width() {
        return this.width;
    }

    public SingleAxis width(Object width) {
        this.width = width;
        return this;
    }

    public Object height() {
        return this.height;
    }

    public SingleAxis height(Object height) {
        this.height = height;
        return this;
    }

    public Object left() {
        return this.left;
    }

    public SingleAxis left(Object left) {
        this.left = left;
        return this;
    }

    public SingleAxis left(Integer left) {
        this.left = left;
        return this;
    }

    public Object top() {
        return this.top;
    }

    public SingleAxis top(Object top) {
        this.top = top;
        return this;
    }

    public Object right() {
        return this.right;
    }

    public SingleAxis right(Object right) {
        this.right = right;
        return this;
    }

    public Object bottom() {
        return this.bottom;
    }

    public SingleAxis bottom(Object bottom) {
        this.bottom = bottom;
        return this;
    }

    /**
     * 获取orient值
     */
    public String orient() {
        return this.orient;
    }

    /**
     * 设置orient值
     *
     * @param orient
     */
    public SingleAxis orient(String orient) {
        this.orient = orient;
        return this;
    }

    /**
     * 获取orient值
     */
    public String getOrient() {
        return orient;
    }

    /**
     * 设置orient值
     *
     * @param orient
     */
    public void setOrient(String orient) {
        this.orient = orient;
    }
}
