/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.series.base.RippleEffect;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class EffectScatter extends Series {
    private Boolean legendHoverLink;
    private String effectType;
    private String showEffectOn;
    private RippleEffect rippleEffect;
    private String coordinateSystem;
    private Integer xAxisIndex;
    private Integer yAxisIndex;
    private Integer polarIndex;
    private Integer geoIndex;
    private Object symbol;
    private Object symbolSize;
    private Double symbolRotate;
    private Object symbolOffset;
    private Boolean symbolKeepAspect;
    private Integer calendarIndex;

    public EffectScatter() {
        type = "effectScatter";
    }

    public Boolean symbolKeepAspect() {
        return this.symbolKeepAspect;
    }

    public EffectScatter symbolKeepAspect(Boolean symbolKeepAspect) {
        this.symbolKeepAspect = symbolKeepAspect;
        return this;
    }

    public Integer calendarIndex() {
        return this.calendarIndex;
    }

    public EffectScatter calendarIndex(Integer calendarIndex) {
        this.calendarIndex = calendarIndex;
        return this;
    }

    public String coordinateSystem() {
        return coordinateSystem;
    }

    public EffectScatter coordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
        return this;
    }

    public Integer xAxisIndex() {
        return xAxisIndex;
    }

    public EffectScatter xAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
        return this;
    }

    public Integer yAxisIndex() {
        return yAxisIndex;
    }

    public EffectScatter yAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
        return this;
    }

    public Integer polarIndex() {
        return polarIndex;
    }

    public EffectScatter polarIndex(Integer polarIndex) {
        this.polarIndex = polarIndex;
        return this;
    }

    public Integer geoIndex() {
        return geoIndex;
    }

    public EffectScatter geoIndex(Integer geoIndex) {
        this.geoIndex = geoIndex;
        return this;
    }

    public Object symbol() {
        return symbol;
    }

    public EffectScatter symbol(Object symbol) {
        this.symbol = symbol;
        return this;
    }

    public Object symbolSize() {
        return symbolSize;
    }

    public EffectScatter symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public Double symbolRotate() {
        return symbolRotate;
    }

    public EffectScatter symbolRotate(Double symbolRotate) {
        this.symbolRotate = symbolRotate;
        return this;
    }

    public Object symbolOffset() {
        return symbolOffset;
    }

    public EffectScatter symbolOffset(Object value) {
            this.symbolOffset = value;
        return this;
    }

    public String effectType() {
        return effectType;
    }

    public EffectScatter effectType(String effectType) {
        this.effectType = effectType;
        return this;
    }

    public Boolean legendHoverLink() {
        return legendHoverLink;
    }

    public EffectScatter legendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
        return this;
    }

    public String showEffectOn() {
        return showEffectOn;
    }

    public EffectScatter showEffectOn(String showEffectOn) {
        this.showEffectOn = showEffectOn;
        return this;
    }

    public RippleEffect rippleEffect() {
        if (rippleEffect == null)
            rippleEffect = new RippleEffect();
        return rippleEffect;
    }

    public EffectScatter rippleEffect(RippleEffect rippleEffect) {
        this.rippleEffect = rippleEffect;
        return this;
    }

    public String getCoordinateSystem() {
        return coordinateSystem;
    }

    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    public Integer getxAxisIndex() {
        return xAxisIndex;
    }

    public void setxAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
    }

    public Integer getyAxisIndex() {
        return yAxisIndex;
    }

    public void setyAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }

    public Integer getPolarIndex() {
        return polarIndex;
    }

    public void setPolarIndex(Integer polarIndex) {
        this.polarIndex = polarIndex;
    }

    public Integer getGeoIndex() {
        return geoIndex;
    }

    public void setGeoIndex(Integer geoIndex) {
        this.geoIndex = geoIndex;
    }

    public Boolean getLegendHoverLink() {
        return legendHoverLink;
    }

    public void setLegendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
    }

    public Object getSymbol() {
        return symbol;
    }

    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    public Double getSymbolRotate() {
        return symbolRotate;
    }

    public void setSymbolRotate(Double symbolRotate) {
        this.symbolRotate = symbolRotate;
    }

    public Object getSymbolOffset() {
        return symbolOffset;
    }

    public void setSymbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
    }

    public String getEffectType() {
        return effectType;
    }

    public void setEffectType(String effectType) {
        this.effectType = effectType;
    }

    public String getShowEffectOn() {
        return showEffectOn;
    }

    public void setShowEffectOn(String showEffectOn) {
        this.showEffectOn = showEffectOn;
    }

    public RippleEffect getRippleEffect() {
        return rippleEffect;
    }

    public void setRippleEffect(RippleEffect rippleEffect) {
        this.rippleEffect = rippleEffect;
    }

    public Boolean getSymbolKeepAspect() {
        return symbolKeepAspect;
    }

    public void setSymbolKeepAspect(Boolean symbolKeepAspect) {
        this.symbolKeepAspect = symbolKeepAspect;
    }

    public Integer getCalendarIndex() {
        return calendarIndex;
    }

    public void setCalendarIndex(Integer calendarIndex) {
        this.calendarIndex = calendarIndex;
    }
}
