/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.threeD;

import java.io.Serializable;

public class DepthOfField implements Serializable {
    /**
     * 是否开启景深。
     */
    private boolean enable;
    /**
     * 初始的焦距，用户可以点击区域自动聚焦。
     */
    private boolean focalDistance;
    /**
     * 完全聚焦的区域范围，在此范围内的物体时完全清晰的，不会有模糊
     */
    private boolean focalRange;
    /**
     * 镜头的F值，值越小景深越浅。
     */
    private int fstop;
    /**
     * 焦外的模糊半径
     */
    private int blurRadius;

    public Boolean enable() {
        return this.enable;
    }
    public DepthOfField enable(Boolean enable) {
        this.enable = enable;
        return this;
    }

    public Boolean focalDistance() {
        return this.focalDistance;
    }
    public DepthOfField focalDistance(Boolean focalDistance) {
        this.focalDistance = focalDistance;
        return this;
    }

    public Boolean focalRange() {
        return this.focalRange;
    }
    public DepthOfField focalRange(Boolean focalRange) {
        this.focalRange = focalRange;
        return this;
    }

    public Integer fstop() {
        return this.fstop;
    }
    public DepthOfField fstop(Integer fstop) {
        this.fstop = fstop;
        return this;
    }

    public Integer blurRadius() {
        return this.blurRadius;
    }
    public DepthOfField blurRadius(Integer blurRadius) {
        this.blurRadius = blurRadius;
        return this;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public boolean isFocalDistance() {
        return focalDistance;
    }

    public void setFocalDistance(boolean focalDistance) {
        this.focalDistance = focalDistance;
    }

    public boolean isFocalRange() {
        return focalRange;
    }

    public void setFocalRange(boolean focalRange) {
        this.focalRange = focalRange;
    }

    public int getFstop() {
        return fstop;
    }

    public void setFstop(int fstop) {
        this.fstop = fstop;
    }

    public int getBlurRadius() {
        return blurRadius;
    }

    public void setBlurRadius(int blurRadius) {
        this.blurRadius = blurRadius;
    }
}
