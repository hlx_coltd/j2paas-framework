/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib;

import cn.easyplatform.web.ext.echarts.lib.support.Point;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Bmap extends Point {
    /**
     * 是否显示地理坐标系组件。
     */
    private Boolean show;
    /**
     * 是否开启鼠标缩放和平移漫游。默认不开启。如果只想要开启缩放或者平移，可以设置成 'scale' 或者 'move'。设置成 true 为都开启
     */
    private Object roam;
    /**
     * 当前视角的中心点，用经纬度表示
     */
    private Object center;
    /**
     * 这个参数用于 scale 地图的长宽比
     */
    private Object aspectScale;
    /**
     * 当前视角的缩放比例
     */
    private Object zoom;

    private Object mapStyle;

    public Object roam() {
        return roam;
    }

    public Bmap roam(Object roam) {
        this.roam = roam;
        return this;
    }

    public Object center() {
        return center;
    }

    public Bmap center(Object center) {
        if (center instanceof String) {
            String[] vals = center.toString().split(",");
            this.center = new Double[]{Double.parseDouble(vals[0]),Double.parseDouble(vals[1])};
        } else
            this.center = center;
        return this;
    }

    public Object aspectScale() {
        return zoom;
    }

    public Bmap aspectScale(Object aspectScale) {
        this.aspectScale = aspectScale;
        return this;
    }

    public Object zoom() {
        return zoom;
    }

    public Bmap zoom(Object zoom) {
        this.zoom = zoom;
        return this;
    }

    public Boolean show() {
        return show;
    }

    public Bmap show(Boolean show) {
        this.show = show;
        return this;
    }

    public Object getMapStyle() {
        return mapStyle;
    }

    public void setMapStyle(Object mapStyle) {
        this.mapStyle = mapStyle;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Object getRoam() {
        return roam;
    }

    public void setRoam(Object roam) {
        this.roam = roam;
    }

    public Object getCenter() {
        return center;
    }

    public void setCenter(Object center) {
        this.center = center;
    }

    public Object getAspectScale() {
        return aspectScale;
    }

    public void setAspectScale(Object aspectScale) {
        this.aspectScale = aspectScale;
    }

    public Object getZoom() {
        return zoom;
    }

    public void setZoom(Object zoom) {
        this.zoom = zoom;
    }
}
