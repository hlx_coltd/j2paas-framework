/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.vm;

import cn.easyplatform.web.ext.echarts.lib.style.TextStyle;
import cn.easyplatform.web.ext.echarts.lib.support.Rang;
import cn.easyplatform.web.ext.echarts.lib.type.Align;
import cn.easyplatform.web.ext.echarts.lib.type.VisualMapType;

import java.io.Serializable;

/**
 * 视觉映射组件，用于进行『视觉编码』，也就是将数据映射到视觉元素（视觉通道）
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class VisualMap implements Serializable {
    /**
     * 类型
     *
     * @see VisualMapType
     */
    private String type;
    /**
     * 最小值
     */
    private Integer min;
    /**
     * 最大值
     */
    private Integer max;
    /**
     * 是否反转 visualMap 组件
     */
    private Boolean inverse;
    /**
     * 数据展示的小数精度
     */
    private Object precision;
    /**
     * 图形的宽度，即长条的宽度
     */
    private Integer itemWidth;
    /**
     * 图形的高度，即长条的高度
     */
    private Integer itemHeight;

    /**
     * 指定组件中手柄和文字的摆放关系
     *
     * @see Align
     */
    private String align;
    /**
     * 两端的文本，如 ['High', 'Low']
     */
    private Object text;
    /**
     * 两端文字主体之间的距离，单位为px
     */
    private Object textGap;
    /**
     * 是否显示 visualMap 组件。如果设置为 false，不会显示，但是数据映射的功能还存在
     */
    private Boolean show;
    /**
     * 指定用数据的『哪个维度』，映射到视觉元素上
     */
    private Object dimension;
    /**
     * 指定取哪个系列的数据，即哪个系列的 series.data
     */
    private Object[] seriesIndex;
    /**
     * 打开 hoverLink 功能时，鼠标悬浮到 visualMap 组件上时，鼠标位置对应的数值 在 图表中对应的图形元素，会高亮
     * 反之，鼠标悬浮到图表中的图形元素上时，在 visualMap 组件的相应位置会有三角提示其所对应的数值
     */
    private Boolean hoverLink;
    /**
     * 定义 在选中范围中 的视觉元素
     */
    private Rang inRange;
    /**
     * 定义 在选中范围外 的视觉元素
     */
    private Rang outOfRange;
    /**
     * visualMap 组件中，控制器 的 inRange outOfRange 设置。如果没有这个 controller 设置，控制器 会使用外层的 inRange outOfRange 设置；如果有这个 controller 设置，则会采用这个设置。适用于一些控制器视觉效果需要特殊定制或调整的场景。
     */
    private Controller controller;
    /**
     * 一级层叠控制
     */
    private Integer zlevel;
    /**
     * 二级层叠控制
     */
    private Integer z;
    /**
     * treemap 组件离容器左侧的距离
     */
    private Object left;
    /**
     * treemap 组件离容器上侧的距离
     */
    private Object top;
    /**
     * treemap 组件离容器右侧的距离
     */
    private Object right;
    /**
     * treemap 组件离容器下侧的距离
     */
    private Object bottom;
    /**
     * 水平（'horizontal'）或者竖直（'vertical'）
     */
    private Object orient;
    /**
     * visualMap-continuous内边距，单位px，默认各方向内边距为5，接受数组分别设定上右下左边距。
     */
    private Object padding;
    /**
     * 背景色
     */
    private String backgroundColor;
    /**
     * 边框颜色
     */
    private String borderColor;
    /**
     * 边框线宽，单位px
     */
    private Integer borderWidth;
    /**
     * 这个配置项，是为了兼容 ECharts2 而存在ECharts3 中已经不推荐使用。它的功能已经移到了 visualMap-continuous.inRange 和 visualMap-continuous.outOfRange 中。
     * 如果要使用，则须注意，color属性中的顺序是由数值 大 到 小，但是 visualMap-continuous.inRange 或 visualMap-continuous.outOfRange 中 color 的顺序，总是由数值 小 到 大。二者不一致。
     */
    private Object color;
    /**
     * visualMap 文字的颜色
     */
    private TextStyle textStyle;
    /**
     * 标签的格式化工具
     * 如果为string，表示模板，例如：aaaa{value}。其中 {value} 是当前的范围边界值。
     * 如果为 Function，表示回调函数，形如
     * formatter: function (value) {
     * return 'aaaa' + value; // 范围标签显示内容。
     * }
     */
    private String formatter;

    public Object padding() {
        return padding;
    }

    public VisualMap padding(Object padding) {
        this.padding = padding;
        return this;
    }

    public String backgroundColor() {
        return backgroundColor;
    }

    public VisualMap backgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public String borderColor() {
        return borderColor;
    }

    public VisualMap borderColor(String borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public Integer borderWidth() {
        return borderWidth;
    }

    public VisualMap borderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public Integer zlevel() {
        return zlevel;
    }

    public VisualMap zlevel(Integer zlevel) {
        this.zlevel = zlevel;
        return this;
    }

    public Integer z() {
        return z;
    }

    public VisualMap z(Integer z) {
        this.z = z;
        return this;
    }

    public Object left() {
        return this.left;
    }

    public VisualMap left(Object left) {
        this.left = left;
        return this;
    }

    public Object right() {
        return this.right;
    }

    public VisualMap right(Object right) {
        this.right = right;
        return this;
    }

    public Object top() {
        return this.top;
    }

    public VisualMap top(Object top) {
        this.top = top;
        return this;
    }

    public Object bottom() {
        return this.bottom;
    }

    public VisualMap bottom(Object bottom) {
        this.bottom = bottom;
        return this;
    }

    public Controller controller() {
        if (controller == null)
            controller = new Controller();
        return controller;
    }

    public VisualMap controller(Controller controller) {
        this.controller = controller;
        return this;
    }

    public Boolean show() {
        return this.show;
    }

    public VisualMap show(Boolean show) {
        this.show = show;
        return this;
    }

    public Boolean hoverLink() {
        return this.hoverLink;
    }

    public Boolean getHoverLink() {
        return hoverLink;
    }

    public void setHoverLink(Boolean hoverLink) {
        this.hoverLink = hoverLink;
    }

    public VisualMap hoverLink(Boolean hoverLink) {
        this.hoverLink = hoverLink;
        return this;
    }

    public Boolean inverse() {
        return this.inverse;
    }

    public VisualMap inverse(Boolean inverse) {
        this.inverse = inverse;
        return this;
    }

    public String type() {
        return this.type;
    }

    public VisualMap type(String type) {
        this.type = type;
        return this;
    }

    public Integer min() {
        return this.min;
    }

    public VisualMap min(Integer min) {
        this.min = min;
        return this;
    }

    public Integer max() {
        return this.max;
    }

    public VisualMap max(Integer max) {
        this.max = max;
        return this;
    }

    public Object precision() {
        return this.precision;
    }

    public VisualMap precision(Object precision) {
        this.precision = precision;
        return this;
    }

    public Integer itemWidth() {
        return this.itemWidth;
    }

    public VisualMap itemWidth(Integer itemWidth) {
        this.itemWidth = itemWidth;
        return this;
    }

    public Integer itemHeight() {
        return this.itemHeight;
    }

    public VisualMap itemHeight(Integer itemHeight) {
        this.itemHeight = itemHeight;
        return this;
    }

    public String align() {
        return this.align;
    }

    public VisualMap align(String align) {
        this.align = align;
        return this;
    }


    public Object dimension() {
        return this.dimension;
    }

    public VisualMap dimension(Object dimension) {
        this.dimension = dimension;
        return this;
    }

    public Object seriesIndex() {
        return this.seriesIndex;
    }

    public VisualMap seriesIndex(Object value) {
       if (value instanceof String)
            seriesIndex = ((String) value).split(";");
        else if (value instanceof Object[])
            seriesIndex = (Object[]) value;
        return this;
    }

    public Rang inRange() {
        if (inRange == null)
            inRange = new Rang();
        return this.inRange;
    }

    public VisualMap inRange(Rang inRange) {
        this.inRange = inRange;
        return this;
    }

    public Rang outOfRange() {
        if (outOfRange == null)
            outOfRange = new Rang();
        return this.outOfRange;
    }

    public VisualMap outOfRange(Rang outOfRange) {
        this.outOfRange = outOfRange;
        return this;
    }

    public Object orient() {
        return this.orient;
    }

    public VisualMap orient(Object orient) {
        this.orient = orient;
        return this;
    }

    public String formatter() {
        return this.formatter;
    }

    public VisualMap formatter(String formatter) {
        this.formatter = formatter;
        return this;
    }

    public TextStyle textStyle() {
        if (this.textStyle == null) {
            this.textStyle = new TextStyle();
        }
        return this.textStyle;
    }

    public VisualMap textStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
        return this;
    }

    public Object color() {
        return this.color;
    }

    public VisualMap color(Object... color) {
        if (color.length == 0 || color[0] == null)
            return this;
        this.color = color;
        return this;
    }

    public Object text() {
        return this.text;
    }

    public VisualMap text(Object... text) {
        if (text.length == 0 || text[0] == null)
            return this;
        this.text = text;
        return this;
    }

    public Object textGap() {
        return this.textGap;
    }

    public VisualMap textGap(Object textGap) {
        this.textGap = textGap;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Object getPrecision() {
        return precision;
    }

    public void setPrecision(Object precision) {
        this.precision = precision;
    }

    public Integer getItemWidth() {
        return itemWidth;
    }

    public void setItemWidth(Integer itemWidth) {
        this.itemWidth = itemWidth;
    }

    public Integer getItemHeight() {
        return itemHeight;
    }

    public void setItemHeight(Integer itemHeight) {
        this.itemHeight = itemHeight;
    }

    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public Object getText() {
        return text;
    }

    public void setText(Object[] text) {
        this.text = text;
    }

    public Object getTextGap() {
        return textGap;
    }

    public void setTextGap(Object[] textGap) {
        this.textGap = textGap;
    }


    public Object getDimension() {
        return dimension;
    }

    public void setDimension(Object dimension) {
        this.dimension = dimension;
    }

    public Object getSeriesIndex() {
        return seriesIndex;
    }

    public void setSeriesIndex(Object[] seriesIndex) {
        this.seriesIndex = seriesIndex;
    }

    public Rang getInRange() {
        return inRange;
    }

    public void setInRange(Rang inRange) {
        this.inRange = inRange;
    }

    public Rang getOutOfRange() {
        return outOfRange;
    }

    public void setOutOfRange(Rang outOfRange) {
        this.outOfRange = outOfRange;
    }

    public Object getOrient() {
        return orient;
    }

    public void setOrient(Object orient) {
        this.orient = orient;
    }

    public String getFormatter() {
        return formatter;
    }

    public void setFormatter(String formatter) {
        this.formatter = formatter;
    }

    public Object getColor() {
        return color;
    }

    public void setColor(Object[] color) {
        this.color = color;
    }

    public TextStyle getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
    }

    public Boolean getInverse() {
        return inverse;
    }

    public void setInverse(Boolean inverse) {
        this.inverse = inverse;
    }

    public void setText(Object text) {
        this.text = text;
    }

    public void setTextGap(Object textGap) {
        this.textGap = textGap;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Controller getController() {
        return controller;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public Integer getZlevel() {
        return zlevel;
    }

    public void setZlevel(Integer zlevel) {
        this.zlevel = zlevel;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    public Object getLeft() {
        return left;
    }

    public void setLeft(Object left) {
        this.left = left;
    }

    public Object getTop() {
        return top;
    }

    public void setTop(Object top) {
        this.top = top;
    }

    public Object getRight() {
        return right;
    }

    public void setRight(Object right) {
        this.right = right;
    }

    public Object getBottom() {
        return bottom;
    }

    public void setBottom(Object bottom) {
        this.bottom = bottom;
    }

    public Object getPadding() {
        return padding;
    }

    public void setPadding(Object padding) {
        this.padding = padding;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public Integer getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
    }

    public void setColor(Object color) {
        this.color = color;
    }

    public static class Controller implements Serializable {
        private Rang inRange;
        private Rang outOfRange;

        public Rang inRange() {
            if (inRange == null)
                inRange = new Rang();
            return this.inRange;
        }

        public Controller inRange(Rang inRange) {
            this.inRange = inRange;
            return this;
        }

        public Rang outOfRange() {
            if (outOfRange == null)
                outOfRange = new Rang();
            return this.outOfRange;
        }

        public Controller outOfRange(Rang outOfRange) {
            this.outOfRange = outOfRange;
            return this;
        }

        public Rang getInRange() {
            return inRange;
        }

        public void setInRange(Rang inRange) {
            this.inRange = inRange;
        }

        public Rang getOutOfRange() {
            return outOfRange;
        }

        public void setOutOfRange(Rang outOfRange) {
            this.outOfRange = outOfRange;
        }
    }
}
