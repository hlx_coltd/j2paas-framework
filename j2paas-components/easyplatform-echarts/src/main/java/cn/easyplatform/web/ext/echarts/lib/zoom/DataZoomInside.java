/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.zoom;

import cn.easyplatform.web.ext.echarts.lib.type.DataZoomType;

/**
 * 数据区域缩放。与toolbox.feature.dataZoom同步，仅对直角坐标系图表有效
 * /**
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DataZoomInside extends DataZoom {

    /**
     * 是否停止组件的功能
     */
    private Boolean disabled;

    private Boolean zoomOnMouseWheel;

    private Boolean moveOnMouseMove;

    private Boolean moveOnMouseWheel;

    private Boolean preventDefaultMouseMove;

    /**
     * 构造函数
     */
    public DataZoomInside() {
        this.type(DataZoomType.inside.name());
    }

    public Boolean preventDefaultMouseMove() {
        return this.preventDefaultMouseMove;
    }

    public DataZoomInside preventDefaultMouseMove(Boolean preventDefaultMouseMove) {
        this.preventDefaultMouseMove = preventDefaultMouseMove;
        return this;
    }

    public Boolean moveOnMouseWheel() {
        return this.moveOnMouseWheel;
    }

    public DataZoomInside moveOnMouseWheel(Boolean moveOnMouseWheel) {
        this.moveOnMouseWheel = moveOnMouseWheel;
        return this;
    }

    public Boolean moveOnMouseMove() {
        return this.moveOnMouseMove;
    }

    public DataZoomInside moveOnMouseMove(Boolean moveOnMouseMove) {
        this.moveOnMouseMove = moveOnMouseMove;
        return this;
    }

    public Boolean zoomOnMouseWheel() {
        return this.zoomOnMouseWheel;
    }

    public DataZoomInside zoomOnMouseWheel(Boolean zoomOnMouseWheel) {
        this.zoomOnMouseWheel = zoomOnMouseWheel;
        return this;
    }

    public Boolean disabled() {
        return this.disabled;
    }

    public DataZoomInside disabled(Boolean disabled) {
        this.disabled = disabled;
        return this;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Boolean getZoomOnMouseWheel() {
        return zoomOnMouseWheel;
    }

    public void setZoomOnMouseWheel(Boolean zoomOnMouseWheel) {
        this.zoomOnMouseWheel = zoomOnMouseWheel;
    }

    public Boolean getMoveOnMouseMove() {
        return moveOnMouseMove;
    }

    public void setMoveOnMouseMove(Boolean moveOnMouseMove) {
        this.moveOnMouseMove = moveOnMouseMove;
    }

    public Boolean getMoveOnMouseWheel() {
        return moveOnMouseWheel;
    }

    public void setMoveOnMouseWheel(Boolean moveOnMouseWheel) {
        this.moveOnMouseWheel = moveOnMouseWheel;
    }

    public Boolean getPreventDefaultMouseMove() {
        return preventDefaultMouseMove;
    }

    public void setPreventDefaultMouseMove(Boolean preventDefaultMouseMove) {
        this.preventDefaultMouseMove = preventDefaultMouseMove;
    }
}
