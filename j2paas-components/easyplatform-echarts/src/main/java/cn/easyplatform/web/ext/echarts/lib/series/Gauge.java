/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.axis.AxisLabel;
import cn.easyplatform.web.ext.echarts.lib.axis.AxisLine;
import cn.easyplatform.web.ext.echarts.lib.axis.AxisTick;
import cn.easyplatform.web.ext.echarts.lib.axis.SplitLine;
import cn.easyplatform.web.ext.echarts.lib.series.base.Pointer;
import cn.easyplatform.web.ext.echarts.lib.series.base.Title;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Gauge extends Series {
    private Object radius;
    private Object startAngle;
    private Object endAngle;
    private Boolean clockwise;
    private Integer min;
    private Integer max;
    private Integer splitNumber;
    private AxisLine axisLine;
    private SplitLine splitLine;
    private AxisTick axisTick;
    private AxisLabel axisLabel;
    private Pointer pointer;
    private Title title;
    private Title detail;

    public Gauge() {
        type = "gauge";
    }

    public AxisLine axisLine() {
        if (axisLine == null)
            axisLine = new AxisLine();
        return axisLine;
    }

    public Gauge axisLine(AxisLine axisLine) {
        this.axisLine = axisLine;
        return this;
    }

    public SplitLine splitLine() {
        if (splitLine == null)
            splitLine = new SplitLine();
        return splitLine;
    }

    public Gauge splitLine(SplitLine splitLine) {
        this.splitLine = splitLine;
        return this;
    }

    public AxisTick axisTick() {
        if (axisTick == null)
            axisTick = new AxisTick();
        return axisTick;
    }

    public Gauge axisTick(AxisTick axisTick) {
        this.axisTick = axisTick;
        return this;
    }

    public AxisLabel axisLabel() {
        if (axisLabel == null)
            axisLabel = new AxisLabel();
        return axisLabel;
    }

    public Gauge axisLabel(AxisLabel axisLabel) {
        this.axisLabel = axisLabel;
        return this;
    }

    public Pointer pointer() {
        if (pointer == null)
            pointer = new Pointer();
        return pointer;
    }

    public Gauge pointer(Pointer pointer) {
        this.pointer = pointer;
        return this;
    }

    public Title title() {
        if (title == null)
            title = new Title();
        return title;
    }

    public Gauge title(Title title) {
        this.title = title;
        return this;
    }

    public Title detail() {
        if (detail == null)
            detail = new Title();
        return detail;
    }

    public Gauge detail(Title detail) {
        this.detail = detail;
        return this;
    }

    public Object min() {
        return min;
    }

    public Gauge min(Integer min) {
        this.min = min;
        return this;
    }

    public Object max() {
        return max;
    }

    public Gauge max(Integer max) {
        this.max = max;
        return this;
    }

    public Object radius() {
        return radius;
    }

    public Gauge radius(Object radius) {
        this.radius = radius;
        return this;
    }

    public Object startAngle() {
        return startAngle;
    }

    public Gauge startAngle(Object startAngle) {
        this.startAngle = startAngle;
        return this;
    }

    public Object endAngle() {
        return endAngle;
    }

    public Gauge endAngle(Object endAngle) {
        this.endAngle = endAngle;
        return this;
    }

    public Object clockwise() {
        return clockwise;
    }

    public Gauge clockwise(Boolean clockwise) {
        this.clockwise = clockwise;
        return this;
    }

    public Object splitNumber() {
        return splitNumber;
    }

    public Gauge splitNumber(Integer splitNumber) {
        this.splitNumber = splitNumber;
        return this;
    }

    public Object getRadius() {
        return radius;
    }

    public void setRadius(Object radius) {
        this.radius = radius;
    }

    public Object getStartAngle() {
        return startAngle;
    }

    public void setStartAngle(Object startAngle) {
        this.startAngle = startAngle;
    }

    public Object getEndAngle() {
        return endAngle;
    }

    public void setEndAngle(Object endAngle) {
        this.endAngle = endAngle;
    }

    public Boolean getClockwise() {
        return clockwise;
    }

    public void setClockwise(Boolean clockwise) {
        this.clockwise = clockwise;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Integer getSplitNumber() {
        return splitNumber;
    }

    public void setSplitNumber(Integer splitNumber) {
        this.splitNumber = splitNumber;
    }

    public AxisLine getAxisLine() {
        return axisLine;
    }

    public void setAxisLine(AxisLine axisLine) {
        this.axisLine = axisLine;
    }

    public SplitLine getSplitLine() {
        return splitLine;
    }

    public void setSplitLine(SplitLine splitLine) {
        this.splitLine = splitLine;
    }

    public AxisTick getAxisTick() {
        return axisTick;
    }

    public void setAxisTick(AxisTick axisTick) {
        this.axisTick = axisTick;
    }

    public AxisLabel getAxisLabel() {
        return axisLabel;
    }

    public void setAxisLabel(AxisLabel axisLabel) {
        this.axisLabel = axisLabel;
    }

    public Pointer getPointer() {
        return pointer;
    }

    public void setPointer(Pointer pointer) {
        this.pointer = pointer;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public Title getDetail() {
        return detail;
    }

    public void setDetail(Title detail) {
        this.detail = detail;
    }
}
