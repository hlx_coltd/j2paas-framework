/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.util;

import cn.easyplatform.web.ext.echarts.ECharts;
import cn.easyplatform.web.ext.echarts.lib.Option;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;
import org.zkoss.util.resource.Locators;

import java.io.File;
import java.util.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public final class EChartsUtils {

    public final static void clear(Option option) {
        if (option.getxAxis() != null) {
            if (option.getxAxis() instanceof Map) {
                Map<String, Object> axis = (Map<String, Object>) option.getxAxis();
                if (axis.containsKey("data"))
                    axis.put("data", "data");
            } else if (option.getxAxis() instanceof List) {
                List<Map<String, Object>> xaxis = (List<Map<String, Object>>) option.getxAxis();
                for (Map<String, Object> axis : xaxis) {
                    if (axis.containsKey("data"))
                        axis.put("data", "data");
                }
            }
        }
        if (option.getyAxis() != null) {
            if (option.getyAxis() instanceof Map) {
                Map<String, Object> axis = (Map<String, Object>) option.getyAxis();
                if (axis.containsKey("data"))
                    axis.put("data", "data");
            } else if (option.getyAxis() instanceof List) {
                List<Map<String, Object>> xaxis = (List<Map<String, Object>>) option.getyAxis();
                for (Map<String, Object> axis : xaxis) {
                    if (axis.containsKey("data"))
                        axis.put("data", "data");
                }
            }
        }
        if (option.getSeries() != null) {
            List<Map<String, Object>> series = (List<Map<String, Object>>) option.getSeries();
            for (Map<String, Object> map : series)
                if (map.containsKey("data"))
                    map.put("data", "data");
        }
        if (option.getDataset() != null) {
            option.getDataset().setSource("data");
        }
    }

    public final static Map getComAttributeValue() {
        Map map = new HashMap();
        List list = new ArrayList();
        try {
            Document doc = new Builder().build(Locators.getDefault()
                    .getResourceAsStream("web/js/echarts/CreateCondition.xml"));
            Elements elements = doc.getRootElement().getChildElements();
            String condition = null;
            for (int i = 0; i < elements.size(); i++) {
                Element element = elements.get(i);
                condition = element.getAttributeValue("name");
                Elements targetElms = element.getChildElements();
                for(int j = 0; j < targetElms.size(); j++){
                    Element targetElm = targetElms.get(j);
                    String name = targetElm.getAttributeValue("name");
                    list.add(name);
                }

            }
            doc.detach();
            doc = null;
            map.put(condition,list);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return map;
    }
}
