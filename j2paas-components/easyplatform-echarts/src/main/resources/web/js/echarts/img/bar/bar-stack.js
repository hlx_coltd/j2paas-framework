﻿{
    tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
            type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    legend: {
        data: ['直接访问', '邮件营销', '联盟广告', '视频广告', '搜索引擎', '百度', '谷歌', '必应', '其他']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: [
        {
            type: 'category',
            data: 'data'
        }
    ],
    yAxis: [
        {
            type: 'value'
        }
    ],
    series: [
        {
            name: '直接访问',
            type: 'bar',
            data: 'data'
        },
        {
            name: '邮件营销',
            type: 'bar',
            stack: '广告',
            data: 'data'
        },
        {
            name: '联盟广告',
            type: 'bar',
            stack: '广告',
            data: 'data'
        },
        {
            name: '视频广告',
            type: 'bar',
            stack: '广告',
            data: 'data'
        },
        {
            name: '搜索引擎',
            type: 'bar',
            data: 'data',
            markLine: {
                lineStyle: {
                    type: 'dashed'
                },
                data: [
                    [{type: 'min'}, {type: 'max'}]
                ]
            }
        },
        {
            name: '百度',
            type: 'bar',
            barWidth: 5,
            stack: '搜索引擎',
            data: 'data'
        },
        {
            name: '谷歌',
            type: 'bar',
            stack: '搜索引擎',
            data: 'data'
        },
        {
            name: '必应',
            type: 'bar',
            stack: '搜索引擎',
            data: 'data'
        },
        {
            name: '其他',
            type: 'bar',
            stack: '搜索引擎',
            data: 'data'
        }
    ]
}