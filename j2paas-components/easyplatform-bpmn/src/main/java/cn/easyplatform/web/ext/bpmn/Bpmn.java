/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.bpmn;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Map;

import org.zkoss.lang.Objects;
import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.ui.HtmlNativeComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.impl.XulElement;

public class Bpmn extends XulElement {

	public static final String ON_Save = "onSave";

	static {
		addClientEvent(Bpmn.class, "onSave", CE_IMPORTANT);
	}


	private String value;
	private boolean readonly;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		if (!Objects.equals(this.value, value)) {
			this.value = value;
			smartUpdate("value", value);
		}
	}

	public boolean setReadonly() {
		return readonly;
	}

	public void setReadonly(boolean readonly) {
		if (!Objects.equals(this.readonly, readonly)) {
			this.readonly = readonly;
			smartUpdate("readonly", readonly);
		}
	}

	//super//
	protected void renderProperties(org.zkoss.zk.ui.sys.ContentRenderer renderer)
	throws java.io.IOException {
		super.renderProperties(renderer);
		render(renderer, "value", value);
		render(renderer, "readonly", readonly);
	}
	
	public void service(AuRequest request, boolean everError) {
		final String cmd = request.getCommand();
		final String data = (String) request.getData().get("data");
		this.setValue(data);
		if (cmd.equals(Events.ON_CLICK)) {
			if(null!=data && ""!=data) {
				Event evt = new Event(cmd, this, data);
				Events.postEvent(evt);
			}
		}else
			super.service(request, everError);
	}

	public String getZclass() {
		return (this._zclass != null ? this._zclass : "z-bpmn");
	}
}

