function (out) {
    if (this._type == 'document') {
        out.push('<div class="document-editor" id="', this.uuid, '"', this.domAttrs_({domStyle: true}), '>');
        out.push('<div class="document-editor__toolbar" id="', this.uuid, '-toolbar"></div>');
        out.push('<div class="document-editor__editable-container">');
        out.push('<div class="document-editor__editable" id="', this.uuid, '-editor">');
        out.push('</div></div></div>')
    } else {
        out.push('<div id="', this.uuid, '"', this.domAttrs_({domStyle: true}), '>');
        out.push('<div id="', this.uuid, '-editor"', this.domAttrs_({domStyle: true}), '></div>');
        out.push('</div>')
    }
}