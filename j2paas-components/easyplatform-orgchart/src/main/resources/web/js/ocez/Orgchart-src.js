ocez.Orgchart = zk.$extends(zk.Widget, {
    _pan: false,
    _zoom: false,
    _direction: 't2b',
    _toggleSiblingsResp: false,
    _depth: 999,
    _parentNodeSymbol: 'users',
    _exportButton: false,
    _exportFilename: 'Orgchart',
    _chartClass: '',
    _dragg: false,
    _callback: null,
    _verticalDepth: null,
    _zoominLimit: 7,
    _zoomoutLimit: 0.5,
    _dropCriteria: null,
    _initCompleted: null,
    _createNode: false,
    $define: {
        data: _zkf = function () {
            if (this.desktop) {
                $('#' + this.uuid).children().remove();
                this._init();
            }
        },
        pan: _zkf,
        zoom: _zkf,
        direction: _zkf,
        toggleSiblingsResp: _zkf,
        depth: _zkf,
        parentNodeSymbol: _zkf,
        createNode: _zkf,
        exportButton: _zkf,
        exportFilename: _zkf,
        chartClass: _zkf,
        dragg: _zkf,
        verticalDepth: _zkf,
        zoominLimit: _zkf,
        zoomoutLimit: _zkf,
        dropCriteria: _zkf,
        initCompleted: _zkf
    },
    setExpandAll: function (data) {
        if (this.desktop) {
            if (data)
                this._data = data;
            this._depth = 999;
            $('#' + this.uuid).children().remove();
            this._init();
        }
    },
    setCollapseAll: function (data) {
        if (this.desktop) {
            this._depth = 2;
            $('#' + this.uuid).children().remove();
            this._init();
        }
    },
    setCallback: function (cb) {
        this._callback = cb;
    },
    setFragment: function (fragment) {
        this._callback.call(this, jq.evalJSON(fragment))
        this.onSize();
    },
    redraw: function (out) {
        out.push('<div', this.domAttrs_({domStyle: true}), ' id="', this.uuid, '"<div></div>');
    },
    bind_: function () {
        this.$supers('bind_', arguments);
        zWatch.listen({onSize: this});
        this._init();
    },
    _init: function () {
        var opts = {
            'id': this.uuid,
            'pan': this._pan,
            'zoom': this._zoom,
            'direction': this._direction,
            'toggleSiblingsResp': this._toggleSiblingsResp,
            'depth': this._depth,
            'nodeTitle': 'name',
            'parentNodeSymbol': this._parentNodeSymbol,
            'nodeContent': 'title',
            'nodeId': 'id',
            'exportButton': this._exportButton,
            'exportFilename': this._exportFilename,
            'chartClass': this._chartClass,
            'draggable': this._dragg,
            'zoominLimit': this._zoominLimit,
            'zoomoutLimit': this._zoomoutLimit,
            'verticalDepth': this._verticalDepth,
            'dropCriteria': this._dropCriteria,
            'initCompleted': this._initCompleted
        };
        if (this._data) {
            jq.extend(opts, {'data': jq.evalJSON(this._data)});
        } else {
            jq.extend(opts, {'data': {}});
        }
        if (this._createNode) {
            var wgt = this;
            jq.extend(opts, {
                'createNode': function ($node, data) {
                    if (data.img && data.img.trim() != '') {
                        var secondMenuIcon = $('<i>', {//带图标
                            'class': 'z-icon-info-circle second-menu-icon',
                            click: function () {
                                $(this).siblings('.second-menu').toggle();
                            }
                        });
                        var secondMenu = '<div class="second-menu"><img class="avatar" src="' + data.img + '"></div>';
                        $node.append(secondMenuIcon).append(secondMenu);
                    }
                    if (wgt._createNode === 2) {//分层,目前还未处理
                        if ($node.is('.drill-down')) {//往下钻取
                            var assoClass = data.className.match(/asso-\w+/)[0];
                            var drillDownIcon = $('<i>', {
                                'class': 'z-icon-arrow-circle-down drill-icon',
                                'click': function () {
                                    $('#chart-container').find('.orgchart:visible').addClass('hidden');
                                    if (!$('#chart-container').find('.orgchart.' + assoClass).length) {
                                        if (wgt.isListen('onOpen'))
                                            wgt.fire("onOpen", {id: data.id, tp: 'layer'}, {sendAhead: true});
                                    } else {
                                        $('#chart-container').find('.orgchart.' + assoClass).removeClass('hidden');
                                    }
                                }
                            });
                            $node.append(drillDownIcon);
                        } else if ($node.is('.drill-up')) {
                            var assoClass = data.className.match(/asso-\w+/)[0];
                            var drillUpIcon = $('<i>', {
                                'class': 'z-icon-arrow-circle-up drill-icon',
                                'click': function () {
                                    $('#chart-container').find('.orgchart:visible').addClass('hidden').end()
                                        .find('.drill-down.' + assoClass).closest('.orgchart').removeClass('hidden');
                                }
                            });
                            $node.append(drillUpIcon);
                        }
                    }
                }
            });
        }
        if (opts.exportButton) {
            var wgt = this;
            jq.getScript(zk.ajaxURI("/web/js/ocez/js/html2canvas.min.js", {au: true}))
                .done(function () {
                    var chart = $('#' + wgt.uuid).orgchart(opts);
                    if (opts.draggable) {
                        chart.$chart.on('nodedropped.orgchart', function (event) {
                            if (wgt.isListen('onDrop'))
                                wgt.fire("onDrop", {
                                    s: event.draggedNode[0].id,
                                    t: event.dropZone[0].id
                                }, {sendAhead: true});
                        });
                    }
                });
        } else {
            var chart = $('#' + this.uuid).orgchart(opts);
            if (opts.draggable) {
                var wgt = this;
                chart.$chart.on('nodedropped.orgchart', function (event) {
                    if (wgt.isListen('onDrop'))
                        wgt.fire("onDrop", {s: event.draggedNode[0].id, t: event.dropZone[0].id}, {sendAhead: true});
                });
            }
        }
        setTimeout(function () {
            wgt.onSize();
            //if (!wgt._pan && !wgt._zoom)
            //     jq(wgt.$n()).find('.orgchart').css("overflow", "auto");
        }, 50);
    },
    unbind_: function () {
        zWatch.unlisten({onSize: this});
        $('#' + this.uuid).remove();
        this.$supers('unbind_', arguments);
    },
    onSize: function () {
        this.$supers(ocez.Orgchart, 'onSize', arguments);
        //if (!this._pan || !this._zoom) {
        //var n = this.$n();
        //jq(n).find('.orgchart').css('width', n.clientWidth + 'px').css('height', n.clientHeight + "px");
        //}
    },
    doDoubleClick_: zk.$void,
    initDrag_: zk.$void
});