/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.exporter.pdf.impl;

import cn.easyplatform.web.exporter.pdf.JoinCellEvent;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;

import java.util.Iterator;
import java.util.LinkedHashSet;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JoinCellEventImpl implements JoinCellEvent, PdfPCellEvent {

	private LinkedHashSet<PdfPCellEvent> _events = new LinkedHashSet<PdfPCellEvent>();
	
	public JoinCellEventImpl(PdfPCellEvent... events) {
		for (PdfPCellEvent event : events) {
			_events.add(event);
		}
	}
	
	@Override
	public void addEvent(PdfPCellEvent event) {
		_events.add(event);
	}
	
	@Override
	public void removeEvent(PdfPCellEvent event) {
		_events.remove(event);
	}

	@Override
	public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases) {
		Iterator<PdfPCellEvent> iterator = _events.iterator();
		while (iterator.hasNext()) {
			iterator.next().cellLayout(cell, position, canvases);
		}
	}
	
}
