/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.exporter.excel.imp;

import cn.easyplatform.lang.Nums;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.datalist.ListHeaderVo;
import cn.easyplatform.utils.HttpUtil;
import cn.easyplatform.utils.Images;
import cn.easyplatform.web.WebApps;
import cn.easyplatform.web.contexts.Contexts;
import cn.easyplatform.web.exporter.excel.CellValueSetter;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import static cn.easyplatform.web.exporter.util.Utils.getStringValue;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CellValueSetterImpl implements CellValueSetter<Component> {

    private final DateParser _dateParser;
    private final DecimalFormatSymbols dfs;

    public CellValueSetterImpl(Locale locale) {
        _dateParser = new DateParser(locale);
        dfs = new DecimalFormatSymbols(locale);
    }

    @Override
    public void setCellValue(ListHeaderVo listheader, Component component,
                             Cell cell) {
        if (component.getFirstChild() instanceof Image)
            parseAndSetCellValueToImage((Image) component.getFirstChild(), cell);
        else
            setCellValue(listheader, getStringValue(component), cell);
    }

    @Override
    public void setCellValue(Component component, Cell cell) {
        setCellValue(null, getStringValue(component), cell);
    }

    private void setCellValue(ListHeaderVo listheader, String value, Cell cell) {
        if ("true".equalsIgnoreCase(value) || "false".equalsIgnoreCase(value)) {
            cell.setCellValue(Boolean.valueOf(value));
        } else if (listheader == null || listheader.getType() == null) {
            parseAndSetCellValueToDoubleDateOrString(value, cell);
        } else {
            switch (listheader.getType()) {
                case CHAR:
                case VARCHAR:
                case CLOB:
                    cell.setCellValue(value);
                    break;
                case LONG:
                case INT:
                case NUMERIC:
                case TIME:
                case DATE:
                case DATETIME:
                    parseAndSetCellValueToDoubleDateOrString(value, cell);
                    break;
                case BOOLEAN:
                    cell.setCellValue(value);
                    break;
                default:
                    parseAndSetCellValueToDoubleDateOrString(value, cell);
            }
        }
    }

    private void parseAndSetCellValueToImage(Image img, Cell cell) {
        XSSFDrawing patriarch = (XSSFDrawing) cell.getSheet().createDrawingPatriarch();
        XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0, cell.getColumnIndex(), cell.getRowIndex(), cell.getColumnIndex(), cell.getRowIndex());
        anchor.setAnchorType(ClientAnchor.AnchorType.DONT_MOVE_DO_RESIZE);
        if (img.getContent() != null) {
            patriarch.createPicture(anchor, cell.getSheet().getWorkbook().addPicture(img.getContent().getByteData(), XSSFWorkbook.PICTURE_TYPE_PNG));
        } else if (!Strings.isBlank(img.getSrc())) {
            String url;
            if (img.getSrc().startsWith("http")) {
                url = img.getSrc();
            } else {
                url = WebApps.getServerUrl() + img.getSrc() + "&token=" + Contexts.getEnv().getSessionId();
            }
            Object content = HttpUtil.get(url);
            if (content instanceof byte[]) {
                if (!Strings.isBlank(img.getHeight()) && !Strings.isBlank(img.getWidth())) {
                    int h = Nums.toInt(StringUtils.substringBefore(img.getHeight(), "px"), 100), w = Nums.toInt(StringUtils.substringBefore(img.getWidth(), "px"), 100);
                    cell.getRow().setHeight((short) h);
                    try (ByteArrayInputStream bis = new ByteArrayInputStream((byte[]) content); ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
                        BufferedImage bufferedImage = ImageIO.read(bis);
                        bufferedImage = Images.zoomScale(bufferedImage, w, h);
                        ImageIO.write(bufferedImage, FilenameUtils.getExtension(img.getSrc()), bos);
                        patriarch.createPicture(anchor, cell.getSheet().getWorkbook().addPicture(bos.toByteArray(), XSSFWorkbook.PICTURE_TYPE_PNG)).resize();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else
                    patriarch.createPicture(anchor, cell.getSheet().getWorkbook().addPicture((byte[]) content, XSSFWorkbook.PICTURE_TYPE_PNG)).resize();
            }
        }
    }

    private void parseAndSetCellValueToDoubleDateOrString(String txt, Cell cell) {
        final char dot = dfs.getDecimalSeparator();
        final char comma = dfs.getGroupingSeparator();
        String txt0 = txt;
        if (dot != '.' || comma != ',') {
            final int dotPos = txt.lastIndexOf(dot);
            txt0 = txt.replace(comma, ',');
            if (dotPos >= 0) {
                txt0 = txt0.substring(0, dotPos) + '.'
                        + txt0.substring(dotPos + 1);
            }
        }

        try {
            final Double val = Double.parseDouble(txt0);
            cell.setCellValue(val);
        } catch (NumberFormatException ex) {
            parseAndSetCellValueToDateOrString(txt, cell);
        }
    }

    private void parseAndSetCellValueToDateOrString(String txt, Cell cell) {
        try {
            _dateParser.parseToDate(txt);
            cell.setCellValue(txt);
        } catch (Exception e) {
            cell.setCellValue(txt);
        }
    }
}