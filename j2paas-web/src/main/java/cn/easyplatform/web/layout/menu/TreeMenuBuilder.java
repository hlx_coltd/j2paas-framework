/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.layout.menu;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.*;
import cn.easyplatform.web.contexts.Contexts;
import cn.easyplatform.web.layout.IMenuBuilder;
import cn.easyplatform.web.listener.RunTaskListener;
import cn.easyplatform.web.utils.PageUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TreeMenuBuilder implements IMenuBuilder {

    private Tree tree;

    final static String NODE_OPEN_SCRIPT = "this.getFirstCell().setIconSclass(this.isOpen()?'z-icon-folder-open':'z-icon-folder')";

    final static String NODE_CLICK_SCRIPT = "this.setOpen(!this.isOpen());event.stop();this.getFirstCell().setIconSclass(this.isOpen()?'z-icon-folder-open':'z-icon-folder')";

    private Component container;

    public TreeMenuBuilder(Tree tree) {
        this.tree = tree;
    }

    @Override
    public void fill(AuthorizationVo av) {
        if (av == null || av.getRoles() == null)
            return;
        LoginVo user = Contexts.getUser();
        /**if (av.getRoles().isEmpty()) {
         if (user.getType() == UserType.TYPE_ADMIN) { // 具有管理员的菜单
         new AdminMenuHandler(tree).build();
         } else {
         tree.setParent(null);
         tree = null;
         }
         } else */if (av.getRoles().size() == 1
                && (av.getAgents() == null || av.getAgents().isEmpty())) {
            this.container = tree.getFellow("gv5container");
            Component parent = tree.getParent();
            tree.setParent(null);
            parent.appendChild(createTree(av.getRoles().get(0), null));
        } else {
            this.container = tree.getFellow("gv5container");
            Component parent = tree.getParent();
            String mold = (String) tree.getAttribute("tabbox");
            tree.setParent(null);
            Tabbox tbox = new Tabbox();
            tbox.setHflex("true");
            tbox.setVflex("true");
            if (mold != null && mold.equals("accordion"))
                tbox.setMold("accordion");
            else
                tbox.setOrient("left");
            tbox.setParent(parent);
            tbox.appendChild(new Tabpanels());
            tbox.appendChild(new Tabs());
            tbox.getTabs().setWidth("20px");
            for (RoleVo rv : av.getRoles()) {
                Tab tab = new Tab();
                tab.setSclass("menu-tab-text");
                tab.setTooltiptext(rv.getName());
                if (mold != null && mold.equals("accordion"))
                    tab.setLabel(rv.getName());
                else
                    tab.setSclass("menu-z-tab-image");
                if (rv.getImage() == null) {
                    tab.setIconSclass("z-icon-user");
                } else {
                    PageUtils.setTaskIcon(tab, rv.getImage());
                }
                tbox.getTabs().appendChild(tab);
                Tabpanel tabpanel = new Tabpanel();
                tabpanel.appendChild(createTree(rv, null));
                tbox.getTabpanels().appendChild(tabpanel);
            }
            if (av.getAgents() != null && !av.getAgents().isEmpty()) {
                for (AgentVo agent : av.getAgents()) {
                    for (RoleVo rv : agent.getRoles()) {
                        Tab tab = new Tab();
                        tab.setSclass("menu-tab-text");
                        tab.setTooltiptext(agent.getName() + ":" + rv.getName());
                        if (mold != null && mold.equals("accordion")) {
                            tab.setLabel(agent.getName() + ":" + rv.getName());
                        } else
                            tab.setSclass("menu-z-tab-image");
                        if (rv.getImage() == null) {
                            tab.setIconSclass("z-icon-user");
                        } else {
                            PageUtils.setTaskIcon(tab, rv.getImage());
                        }
                        tbox.getTabs().appendChild(tab);
                        Tabpanel tabpanel = new Tabpanel();
                        tabpanel.appendChild(createTree(rv, agent));
                        tbox.getTabpanels().appendChild(tabpanel);
                    }
                }
            }
        }
    }

    private Tree createTree(RoleVo role, AgentVo agent) {
        Tree t = new Tree();
        if (!Strings.isBlank(tree.getSclass()))
            t.setSclass(tree.getSclass());
        if (!Strings.isBlank(tree.getStyle()))
            t.setStyle(tree.getStyle());
        t.setVflex(true);
        t.setHflex("true");
        Treechildren children = new Treechildren();
        if (tree.getTreechildren() != null) {
            if (!Strings.isBlank(tree.getTreechildren().getSclass()))
                children.setSclass(tree.getTreechildren().getSclass());
            if (!Strings.isBlank(tree.getTreechildren().getStyle()))
                children.setStyle(tree.getTreechildren().getStyle());
        }
        children.setParent(t);
        for (MenuVo mv : role.getMenus()) {
            createMenu(role, agent, children, mv);
        }
        if (children.getFirstChild() != null) {
            ((Treeitem) children.getFirstChild()).setOpen(true);
            ((Treecell) children.getFirstChild().getFirstChild()
                    .getFirstChild()).setIconSclass("z-icon-folder-open");
        }
        return t;
    }

    private void createMenu(RoleVo role, AgentVo agent,
                            HtmlBasedComponent parent, MenuVo mv) {
        Treeitem ti = new Treeitem();
        ti.setAttribute("id", mv.getId());
        ti.setParent(parent);
        ti.setOpen(false);
        Treerow tr = new Treerow();
        tr.setParent(ti);
        Treecell tc = new Treecell(mv.getName());
        tc.setTooltiptext(mv.getName());
        tc.setParent(tr);
        if (!Strings.isBlank(mv.getImage()))
            PageUtils.setTaskIcon(tc, mv.getImage());
        else
            tc.setIconSclass("z-icon-folder");
        ti.setWidgetListener(Events.ON_OPEN, NODE_OPEN_SCRIPT);
        ti.setWidgetListener(Events.ON_CLICK, NODE_CLICK_SCRIPT);
        Treechildren children = new Treechildren();
        children.setParent(ti);
        if (mv.getChildMenus() != null && !mv.getChildMenus().isEmpty()) {
            for (MenuVo vo : mv.getChildMenus()) {
                createMenu(role, agent, children, vo);
            }
        }
        if (mv.getTasks() != null && !mv.getTasks().isEmpty()) {
            for (MenuNodeVo tv : mv.getTasks()) {
                createTask(role, agent, children, tv);
            }
        }
    }

    /**
     * @param parent
     * @param tv
     */
    private void createTask(RoleVo role, AgentVo agent,
                            HtmlBasedComponent parent, MenuNodeVo tv) {
        Treeitem ti = new Treeitem();
        ti.setAttribute("id", tv.getId());
        tv.setRoleId(role.getId());
        if (agent != null)
            tv.setAgent(agent.getId());
        ti.setParent(parent);
        Treerow tr = new Treerow();
        tr.setParent(ti);
        Treecell tc = new Treecell(tv.getName());
        tc.setTooltiptext(tv.getDesp() == null ? tv.getName() : tv.getDesp());
        tc.setParent(tr);
        PageUtils.setTaskIcon(tc, tv.getImage());
        ti.addEventListener(Events.ON_CLICK, new RunTaskListener(tv, this.container));
    }
}
