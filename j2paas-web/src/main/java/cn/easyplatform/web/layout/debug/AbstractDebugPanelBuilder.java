/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.layout.debug;

import cn.easyplatform.lang.Files;
import cn.easyplatform.lang.Lang;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.DebugRequestMessage;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.GetSourceResponseMessage;
import cn.easyplatform.messages.response.GetTaskRuntimeResponeMessage;
import cn.easyplatform.messages.vos.GetSourceVo;
import cn.easyplatform.messages.vos.ListVo;
import cn.easyplatform.messages.vos.TaskRuntimeVo;
import cn.easyplatform.spi.service.ApplicationService;
import cn.easyplatform.spi.service.TaskService;
import cn.easyplatform.type.FieldVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.utils.SerializationUtils;
import cn.easyplatform.web.contexts.Contexts;
import cn.easyplatform.web.dialog.MessageBox;
import cn.easyplatform.web.dialog.SourceViewDlg;
import cn.easyplatform.web.layout.IDebugPanelBuilder;
import cn.easyplatform.web.listener.MessageEventListener;
import cn.easyplatform.web.log.LogManager;
import cn.easyplatform.web.message.entity.Message;
import cn.easyplatform.web.service.ServiceLocator;
import cn.easyplatform.web.utils.PageUtils;
import cn.easyplatform.web.utils.TaskInfo;
import org.apache.commons.lang3.ArrayUtils;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkmax.zul.Filedownload;
import org.zkoss.zul.*;

import java.io.IOException;
import java.io.Reader;
import java.util.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class AbstractDebugPanelBuilder implements IDebugPanelBuilder,
        EventListener<Event> {

    private static String _content;

    public final static Map<String, String> systemProperties = new HashMap<String, String>();

    public static List<String> sourceViews = null;

    static {
        _content = Streams.readAndClose(Streams.utf8r(Files
                .findFileAsStream("web/pages/debug.zul")));
        Properties props = new Properties();
        try {
            Reader is = Streams.utf8r(Files
                    .findFileAsStream("web/pages/system.properties"));
            props.load(is);
            Streams.safeClose(is);
            for (Map.Entry<Object, Object> entry : props.entrySet()) {
                if (entry.getKey().equals("1000"))
                    sourceViews = Lang.list(entry.getValue().toString()
                            .split(","));
                else
                    systemProperties.put(entry.getKey().toString(), entry
                            .getValue().toString());
            }
            props = null;
        } catch (IOException e) {
            Lang.wrapThrow(e);
        }
    }

    private Grid systemGrid;

    private Grid fieldGrid;

    private Grid userGrid;

    private Grid listGrid;

    private Tabbox tabbox;

    private EventListener<Event> consoleSubscriber;

    private String currentId;

    public Component build(Component target) {
        // StringBuilder sb = new StringBuilder(ApplicationContext.me()
        // .getWorkPath());
        // sb.append("/WEB-INF/page/debug.zul");
        // _content = Streams.readAndClose(Streams.fileInr(sb.toString()));
        tabbox = (Tabbox) Executions.createComponentsDirectly(_content, "zul",
                null, null);
        tabbox.setPage(target.getPage());
        Iterator<Component> itr = tabbox.getFellows().iterator();
        while (itr.hasNext()) {
            Component comp = itr.next();
            if (comp.getId().equals("gv5debugsystem"))
                systemGrid = (Grid) comp;
            else if (comp.getId().equals("gv5debugfield"))
                fieldGrid = (Grid) comp;
            else if (comp.getId().equals("gv5debuguser"))
                userGrid = (Grid) comp;
            else if (comp.getId().equals("gv5debugdatalist"))
                listGrid = (Grid) comp;
            else if (comp instanceof Button)
                comp.addEventListener(Events.ON_CLICK, this);
        }
        tabbox.addEventListener(Events.ON_SELECT, this);
        return tabbox;
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getTarget() instanceof A) {
            String sid = (String) event.getTarget().getAttribute("value");
            IResponseMessage<?> resp = ServiceLocator.lookup(
                    TaskService.class).getSource(
                    new SimpleRequestMessage(currentId, sid));
            if (!resp.isSuccess())
                MessageBox.showMessage(resp);
            else {
                GetSourceVo gsv = ((GetSourceResponseMessage) resp).getBody();
                gsv.setTitle(systemProperties.get(sid));
                new SourceViewDlg(gsv, event.getPage());
            }
        } else if (event.getName().equals(Events.ON_SELECT)) {
            boolean selected = tabbox.getSelectedIndex() >= DebugRequestMessage.DEBUG_TYPE;
            Component comp = event.getTarget().getFellow("gv5debugrefresh");
            comp.setVisible(!selected);
            comp = comp.getNextSibling();//gv5debugtask
            comp.setVisible(!selected);
            comp = comp.getNextSibling();//gv5debuger
            comp.setVisible(selected);
            Button btn = (Button) comp;
            if (btn.getIconSclass().equals("z-icon-stop")) {//正在运行
                Integer type = (Integer) comp.getAttribute("type");
                ServiceLocator.lookup(ApplicationService.class).debug(
                        new DebugRequestMessage(false, type));
                clear(type);
                btn.setIconSclass("z-icon-play");
                btn.setLabel(Labels.getLabel("debug.log.start"));
            }
            comp = comp.getNextSibling();//gv5debugclear
            comp.setVisible(selected);
            ((Button) comp.getNextSibling()).setDisabled(!selected);
        } else if (event.getTarget() instanceof Button) {
            String id = event.getTarget().getId();
            if (id.equals("gv5debugclose"))
                close();
            else if (id.equals("gv5debugexport"))
                export();
            else if (id.equals("gv5debugtask"))
                createRunTaskMenu(event.getTarget());
            else if (id.equals("gv5debugclear")) {
                Html console = null;
                if (tabbox.getSelectedIndex() == DebugRequestMessage.DEBUG_TYPE)
                    console = (Html) tabbox.getFellow("gv5debuglog");
                else
                    console = (Html) tabbox.getFellow("gv5debugconsole");
                console.setContent("$$clear$$");
            } else if (id.equals("gv5debuger")) {
                Button btn = (Button) event.getTarget();
                boolean enabled = btn.getIconSclass().equals("z-icon-play") ? true
                        : false;
                if (tabbox.getSelectedIndex() == DebugRequestMessage.DEBUG_TYPE)
                    debug(enabled);
                else
                    log(enabled);
                if (!enabled) {
                    btn.setIconSclass("z-icon-play");
                    btn.setLabel(Labels.getLabel("debug.log.start"));
                } else {
                    btn.setAttribute("type", tabbox.getSelectedIndex());
                    btn.setIconSclass("z-icon-stop");
                    btn.setLabel(Labels.getLabel("debug.log.stop"));
                }
            } else if (id.equals("gv5debugrefresh")) {
                if (currentId != null)
                    createPanel(currentId);
            }
        } else if (event.getTarget() instanceof Menuitem) {
            Menuitem mi = (Menuitem) event.getTarget();
            createPanel(mi.getValue());
        } else if (event.getName().equals(Events.ON_CLOSE))
            close();
    }

    private void debug(boolean enabled) {
        ServiceLocator.lookup(ApplicationService.class).debug(
                new DebugRequestMessage(enabled, DebugRequestMessage.DEBUG_TYPE));
        if (enabled) {
            Html console = (Html) tabbox.getFellow("gv5debuglog");
            consoleSubscriber = new MessageEventListener(console);
            Contexts.subscribe(Message.CONSOLE, consoleSubscriber);
        } else
            clear(DebugRequestMessage.DEBUG_TYPE);
    }

    private void export() {
        Html console = null;
        if (tabbox.getSelectedIndex() == DebugRequestMessage.DEBUG_TYPE)
            console = (Html) tabbox.getFellow("gv5debuglog");
        else
            console = (Html) tabbox.getFellow("gv5debugconsole");
        if (!Strings.isBlank(console.getContent())) {
            StringBuilder sb = new StringBuilder();
            sb.append("<head>")
                    .append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />")
                    .append("</head>").append(console.getContent());
            AMedia media = new AMedia("debug.html", "html", "text/html",
                    sb.toString());
            sb = null;
            Filedownload.save(media);
        }
    }

    private void log(boolean enabled) {
        ServiceLocator.lookup(ApplicationService.class).debug(
                new DebugRequestMessage(enabled, DebugRequestMessage.LOG_TYPE));
        if (enabled) {
            Html console = (Html) tabbox.getFellow("gv5debugconsole");
            consoleSubscriber = new MessageEventListener(console);
            Contexts.subscribe(Message.LOG, consoleSubscriber);
            LogManager.startConsole();
        } else
            clear(DebugRequestMessage.LOG_TYPE);
    }

    private void createRunTaskMenu(Component comp) {
        Component parent = comp.getFellow("gv5debugtasks");
        parent.getChildren().clear();
        Map<String, Object> repo = Contexts.getDesktop().getAttributes();
        for (Object o : repo.values()) {
            if (o instanceof TaskInfo) {
                TaskInfo ti = (TaskInfo) o;
                if (ti.getChildren() != null && !ti.getChildren().isEmpty()) {
                    Menu menu = new Menu();
                    menu.setLabel(ti.getTitle());
                    PageUtils.setTaskIcon(menu, ti.getImage());
                    parent.appendChild(menu);
                    Menupopup popup = new Menupopup();
                    popup.setParent(menu);
                    Menuitem mi = new Menuitem();
                    mi.setLabel(Labels.getLabel("debug.main.task"));
                    mi.setTooltiptext(ti.getTaskId());
                    PageUtils.setTaskIcon(mi, ti.getImage());
                    mi.setValue(ti.getId());
                    mi.addEventListener(Events.ON_CLICK, this);
                    popup.appendChild(mi);
                    for (TaskInfo child : ti.getChildren()) {
                        mi = new Menuitem();
                        mi.setLabel(child.getTitle());
                        mi.setTooltiptext(child.getTaskId());
                        PageUtils.setTaskIcon(mi, child.getImage());
                        mi.setValue(child.getId());
                        mi.addEventListener(Events.ON_CLICK, this);
                        popup.appendChild(mi);
                    }
                } else {
                    Menuitem mi = new Menuitem();
                    mi.setLabel(ti.getTitle());
                    mi.setTooltiptext(ti.getTaskId());
                    PageUtils.setTaskIcon(mi, ti.getImage());
                    mi.setValue(ti.getId());
                    mi.addEventListener(Events.ON_CLICK, this);
                    parent.appendChild(mi);
                }
            }
        }
    }

    private void createPanel(String id) {
        ApplicationService tc = ServiceLocator
                .lookup(ApplicationService.class);
        IResponseMessage<?> resp = tc.getRuntime(new SimpleRequestMessage(id,
                null));
        if (resp.isSuccess() && resp instanceof GetTaskRuntimeResponeMessage) {
            TaskRuntimeVo trv = ((GetTaskRuntimeResponeMessage) resp).getBody();
            listGrid.getRows().getChildren().clear();
            userGrid.getRows().getChildren().clear();
            systemGrid.getRows().getChildren().clear();
            fieldGrid.getRows().getChildren().clear();
            createSystemPanel(trv.getSystemVars());
            if (trv.getFieldVars() != null)
                createFieldPanel(trv.getFieldVars());
            if (trv.getUserVars() != null)
                createUserPanel(trv.getUserVars());
            if (trv.getListVars() != null)
                createListPanel(trv.getListVars());
            this.currentId = id;
        }
    }

    private void createListPanel(List<ListVo> vars) {
        for (ListVo lv : vars) {
            Row row = new Row();
            Label label = new Label(lv.getId());
            label.setTooltiptext(label.getValue());
            row.appendChild(label);
            label = new Label(lv.getType());
            label.setTooltiptext(label.getValue());
            row.appendChild(label);
            A a = new A(lv.getEntity());
            a.setTooltiptext(label.getValue());
            a.setAttribute("value", lv.getEntity());
            a.addEventListener(Events.ON_CLICK, this);
            row.appendChild(a);
            if (Strings.isBlank(lv.getTableId()))
                row.appendChild(new Label());
            else {
                a = new A(lv.getTableId());
                a.setTooltiptext(label.getValue());
                a.setAttribute("value", lv.getTableId());
                a.addEventListener(Events.ON_CLICK, this);
                row.appendChild(a);
            }
            label = new Label(lv.getName());
            label.setTooltiptext(label.getValue());
            row.appendChild(label);
            label = new Label(lv.getSql() == null ? "" : lv.getSql());
            label.setTooltiptext(label.getValue());
            row.appendChild(label);
            label = new Label(lv.getOrderBy() == null ? "" : lv.getOrderBy());
            label.setTooltiptext(label.getValue());
            row.appendChild(label);
            label = new Label(lv.getParams() == null ? "" : lv.getParams());
            label.setTooltiptext(label.getValue());
            row.appendChild(label);
            listGrid.getRows().appendChild(row);
        }
    }

    private void createSystemPanel(Map<String, Object> vars) {
        Iterator<Map.Entry<String, Object>> itr = new TreeMap<String, Object>(
                vars).entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<String, Object> entry = itr.next();
            Row row = new Row();
            createSystemRow(entry, row);
            if (itr.hasNext())
                createSystemRow(itr.next(), row);
            else {
                row.appendChild(new Label());
                row.appendChild(new Label());
                row.appendChild(new Label());
            }
            systemGrid.getRows().appendChild(row);
        }
    }

    private void createSystemRow(Map.Entry<String, Object> entry, Row row) {
        row.appendChild(new Label(entry.getKey().toString()));
        String desp = systemProperties.get(entry.getKey());
        if (desp == null)
            desp = "";
        Label label = new Label(desp);
        label.setTooltiptext(desp);
        row.appendChild(label);
        if (entry.getValue() != null) {
            if (sourceViews.contains(entry.getKey())) {
                A a = new A(entry.getValue().toString());
                a.setTooltiptext(entry.getValue().toString());
                a.setParent(row);
                a.addEventListener(Events.ON_CLICK, this);
                a.setAttribute("value", entry.getKey());
            } else {
                label = new Label(entry.getValue().toString());
                label.setTooltiptext(entry.getValue().toString());
                row.appendChild(label);
            }
        } else
            row.appendChild(new Label());
    }

    private void createFieldPanel(List<FieldVo> fields) {
        Collections.sort(fields, new FieldComprator());
        Iterator<FieldVo> itr = fields.iterator();
        while (itr.hasNext()) {
            FieldVo fv = itr.next();
            Row row = new Row();
            createFieldRow(fv, row);
            if (itr.hasNext())
                createFieldRow(itr.next(), row);
            fieldGrid.getRows().appendChild(row);
        }
    }

    private void createUserPanel(List<FieldVo> fields) {
        Collections.sort(fields, new FieldComprator());
        Iterator<FieldVo> itr = fields.iterator();
        while (itr.hasNext()) {
            FieldVo fv = itr.next();
            Row row = new Row();
            createFieldRow(fv, row);
            if (itr.hasNext())
                createFieldRow(itr.next(), row);
            userGrid.getRows().appendChild(row);
        }
    }

    private void createFieldRow(FieldVo fv, Row row) {
        Label label = new Label(fv.getName());
        label.setTooltiptext(fv.getName());
        row.appendChild(label);
        label = new Label(fv.getType().toString());
        label.setTooltiptext(label.getValue());
        row.appendChild(label);
        label = new Label(fv.getLength() + "");
        label.setTooltiptext(label.getValue());
        row.appendChild(label);
        label = new Label(fv.getDescription() == null ? ""
                : fv.getDescription());
        label.setTooltiptext(label.getValue());
        row.appendChild(label);
        label = new Label();
        if (fv.getValue() != null) {
            Object val = fv.getValue();
            if (val instanceof byte[]) {
                try {
                    val = SerializationUtils
                            .deserialize((byte[]) fv.getValue());
                } catch (Exception ex) {
                }
            }
            if (val instanceof Object[])
                label.setValue(ArrayUtils.toString(val));
            else
                label.setValue(val.toString());
        }
        label.setTooltiptext(label.getValue());
        row.appendChild(label);
    }

    protected void close() {
        clear(0);
        ServiceLocator.lookup(ApplicationService.class).debug(
                new DebugRequestMessage(false, 0));
    }

    private void clear(int type) {
        if (consoleSubscriber != null) {
            if (type == 0) {
                Contexts.unsubscribe(Message.LOG, consoleSubscriber);
                Contexts.unsubscribe(Message.CONSOLE, consoleSubscriber);
                LogManager.stopConsole();
            } else if (type == DebugRequestMessage.LOG_TYPE) {
                Contexts.unsubscribe(Message.LOG, consoleSubscriber);
                LogManager.stopConsole();
            } else {
                Contexts.unsubscribe(Message.CONSOLE, consoleSubscriber);
            }
            consoleSubscriber = null;
        }
    }
}
