/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.dos;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class LogDo {

	public final static int TYPE_USER = 1;

	public final static int TYPE_TASK = 2;

	public final static int TYPE_TABLE = 3;

	public final static int LEVEL_USER = 1;

	public final static int LEVEL_TASK = 2;

	public final static int LEVEL_TABLE = 3;

	private long id;

	private String userId;

	private int type;

	private String event;

	private String content;

	private String deviceType;

	/**
	 * @param userId
	 * @param type
	 * @param event
	 * @param content
	 */
	public LogDo(long id, String userId, String deviceType, int type,
			String event, String content) {
		this.id = id;
		this.userId = userId;
		this.type = type;
		this.event = event;
		this.content = content;
		this.deviceType = deviceType;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @return the event
	 */
	public String getEvent() {
		return event;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}

}
