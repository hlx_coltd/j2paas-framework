/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.transform;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.EntityInfo;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface ITransformer {

	<T extends BaseEntity> T transformFromXml(EntityInfo entity);
	
	/**
	 * 从输入流中的xml内容转化为java对象
	 * 
	 * @param clazz
	 * @param is
	 * @return
	 * @throws TransformException
	 */
	<T extends BaseEntity> T transformFromXml(Class<T> clazz, InputStream is);

	/**
	 * 把java对象转化为xml格式输出
	 * 
	 * @param object
	 * @param os
	 * @throws TransformException
	 */
	<T extends BaseEntity> void transformToXml(T object, OutputStream os);
}
