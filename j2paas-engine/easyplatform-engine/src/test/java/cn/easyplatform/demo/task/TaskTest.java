/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo.task;

import cn.easyplatform.EasyPlatformRuntimeException;
import cn.easyplatform.dao.utils.DaoUtils;
import cn.easyplatform.demo.AbstractDemoTest;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.transaction.jdbc.JdbcTransactions;
import cn.easyplatform.type.EntityType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TaskTest extends AbstractDemoTest {

	public void test() {
		test1();
	}

	private void test1() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			log.debug("正在创建demo功能");
			conn = JdbcTransactions.getConnection(ds1);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.category");
			pstmt.setString(2, "类型新增");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("task1.xml")));
			pstmt.setString(5, task1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.goods");
			pstmt.setString(2, "功能事件");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task2 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("task2.xml")));
			pstmt.setString(5, task2);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.goods.d1");
			pstmt.setString(2, "普通查询");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task3 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("task3.xml")));
			pstmt.setString(5, task3);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.goods.d2");
			pstmt.setString(2, "多层表头");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task4 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("task4.xml")));
			pstmt.setString(5, task4);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.goods.d3");
			pstmt.setString(2, "自定义查询");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task5 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("task5.xml")));
			pstmt.setString(5, task5);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.goods.d4");
			pstmt.setString(2, "可编辑列表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task6 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("task6.xml")));
			pstmt.setString(5, task6);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.goods.d5");
			pstmt.setString(2, "单元格计算、风格及事件");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task7 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("task7.xml")));
			pstmt.setString(5, task7);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.goods.d6");
			pstmt.setString(2, "简单列表页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task8 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("task8.xml")));
			pstmt.setString(5, task8);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.customer");
			pstmt.setString(2, "普通页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task9 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("customer.xml")));
			pstmt.setString(5, task9);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.customer.d1");
			pstmt.setString(2, "主副表页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task10 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("customer_list.xml")));
			pstmt.setString(5, task10);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.customer.d2");
			pstmt.setString(2, "主副表页面-1");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task11 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("customer_order.xml")));
			pstmt.setString(5, task11);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.cust_order");
			pstmt.setString(2, "主副表页面-1");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task12 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("cust_order.xml")));
			pstmt.setString(5, task12);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.customer.d3");
			pstmt.setString(2, "主副表页面-编辑");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task13 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("customer_list_1.xml")));
			pstmt.setString(5, task13);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.customer.d4");
			pstmt.setString(2, "主副表页面-1");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task14 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("customer_order_1.xml")));
			pstmt.setString(5, task14);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.pie3d.1");
			pstmt.setString(2, "图表与主表数据集相同");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task15 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("pie3d_1.xml")));
			pstmt.setString(5, task15);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.pie3d.2");
			pstmt.setString(2, "图表与主表数据集不同");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task16 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("pie3d_2.xml")));
			pstmt.setString(5, task16);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.crosstab.1");
			pstmt.setString(2, "交叉表及子表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task17 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("crosstab_1.xml")));
			pstmt.setString(5, task17);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.subreport.1");
			pstmt.setString(2, "报表事件(导出、打印)");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task18 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("subreport_1.xml")));
			pstmt.setString(5, task18);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.common.report");
			pstmt.setString(2, "通用报表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task19 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("common_report.xml")));
			pstmt.setString(5, task19);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.report.1");
			pstmt.setString(2, "列表报表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task20 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("datalist_report_1.xml")));
			pstmt.setString(5, task20);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.report.2");
			pstmt.setString(2, "主副报表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task21 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("datalist_report_2.xml")));
			pstmt.setString(5, task21);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.custom.report.1");
			pstmt.setString(2, "自定义报表及逻辑计算");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task22 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("custom_report_1.xml")));
			pstmt.setString(5, task22);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.user.leave");
			pstmt.setString(2, "请假测试");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String user_leave = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("user_leave.xml")));
			pstmt.setString(5, user_leave);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.bpm.user.leave");
			pstmt.setString(2, "条件分支");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String bpm_user_leave = Streams
					.readAndClose(Streams.utf8r(getClass().getResourceAsStream(
							"bpm_user_leave.xml")));
			pstmt.setString(5, bpm_user_leave);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.wf_task1");
			pstmt.setString(2, "待办事项");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String wf_task1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("wf_task1.xml")));
			pstmt.setString(5, wf_task1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.wf_task2");
			pstmt.setString(2, "协办事项");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String wf_task2 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("wf_task2.xml")));
			pstmt.setString(5, wf_task2);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.wf_task3");
			pstmt.setString(2, "抄送任务");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String wf_task3 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("wf_task3.xml")));
			pstmt.setString(5, wf_task3);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.bpm.view");
			pstmt.setString(2, "流程状态");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String bpm_view = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("bpm_view.xml")));
			pstmt.setString(5, bpm_view);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.bpm.forkjoin");
			pstmt.setString(2, "多任务并行");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String forkjoin = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("bpm_forkjoin.xml")));
			pstmt.setString(5, forkjoin);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.bpm.actorall");
			pstmt.setString(2, "多用户会签");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String actorall = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("bpm_actorall.xml")));
			pstmt.setString(5, actorall);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.bpm.test");
			pstmt.setString(2, "流程测试案例");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String bpm_test = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("bpm_test.xml")));
			pstmt.setString(5, bpm_test);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.bpm.subprocess");
			pstmt.setString(2, "子流程控制");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String subprocess = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("bpm_subprocess.xml")));
			pstmt.setString(5, subprocess);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.bpm.order1");
			pstmt.setString(2, "流程实例");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String bpm_order1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("wf_order1.xml")));
			pstmt.setString(5, bpm_order1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.charts.1");
			pstmt.setString(2, "图表和事件");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String charts1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("charts_1.xml")));
			pstmt.setString(5, charts1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.charts.2");
			pstmt.setString(2, "动态图表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String charts2 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("charts_2.xml")));
			pstmt.setString(5, charts2);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.charts.3");
			pstmt.setString(2, "组合图表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String charts3 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("charts_3.xml")));
			pstmt.setString(5, charts3);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.batch.1");
			pstmt.setString(2, "单个功能");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String batch1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("batch_1.xml")));
			pstmt.setString(5, batch1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.batch.2");
			pstmt.setString(2, "串接报表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String batch2 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("batch_2.xml")));
			pstmt.setString(5, batch2);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.batch.3");
			pstmt.setString(2, "串接图表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String batch3 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("batch_3.xml")));
			pstmt.setString(5, batch3);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.batch.4");
			pstmt.setString(2, "串接页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String batch4 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("batch_4.xml")));
			pstmt.setString(5, batch4);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.batch.5");
			pstmt.setString(2, "页面中间");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String batch5 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("batch_5.xml")));
			pstmt.setString(5, batch5);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.batch.6");
			pstmt.setString(2, "自已滚自已");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String batch6 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("batch_6.xml")));
			pstmt.setString(5, batch6);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.batch.7");
			pstmt.setString(2, "多个串接");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String batch7 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("batch_7.xml")));
			pstmt.setString(5, batch7);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.action.1");
			pstmt.setString(2, "列表页面操作");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String taskaction = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("page_action.xml")));
			pstmt.setString(5, taskaction);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.complex.1");
			pstmt.setString(2, "多功能页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String taskcomplex = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("page_complex.xml")));
			pstmt.setString(5, taskcomplex);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.page.load");
			pstmt.setString(2, "数据来源于load函数");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task23 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("customer_load.xml")));
			pstmt.setString(5, task23);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.datalist.load");
			pstmt.setString(2, "数据来源于load函数");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String task24 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("datalist_load.xml")));
			pstmt.setString(5, task24);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.tree.1");
			pstmt.setString(2, "普通分组");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String tree1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree1.xml")));
			pstmt.setString(5, tree1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.tree.2");
			pstmt.setString(2, "编辑");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String tree2 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree2.xml")));
			pstmt.setString(5, tree2);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.tree.3");
			pstmt.setString(2, "主副编辑1");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String tree3 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree3.xml")));
			pstmt.setString(5, tree3);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.tree.4");
			pstmt.setString(2, "主副编辑2");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String tree4 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree4.xml")));
			pstmt.setString(5, tree4);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.tree.5");
			pstmt.setString(2, "同表上下层关系");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String tree5 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree5.xml")));
			pstmt.setString(5, tree5);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.tree.6");
			pstmt.setString(2, "可编辑的上下层关系");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String tree6 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree6.xml")));
			pstmt.setString(5, tree6);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();
			
			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.group.1");
			pstmt.setString(2, "简单报表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String group1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("group1.xml")));
			pstmt.setString(5, group1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.component.1");
			pstmt.setString(2, "扩展控件");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String component1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("component.xml")));
			pstmt.setString(5, component1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.component.2");
			pstmt.setString(2, "外汇报价");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String component2 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("canvas.xml")));
			pstmt.setString(5, component2);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.component.3");
			pstmt.setString(2, "组织结构图");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String component3 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("ecotree.xml")));
			pstmt.setString(5, component3);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.task.list.batch");
			pstmt.setString(2, "批量更新");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TASK.getName());
			String listbatch = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("listbatch.xml")));
			pstmt.setString(5, listbatch);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();
			
			log.debug("创建demo功能成功");
		} catch (SQLException ex) {
			throw new EasyPlatformRuntimeException("TaskTest", ex);
		} finally {
			DaoUtils.closeQuietly(pstmt);
		}
	}
}
