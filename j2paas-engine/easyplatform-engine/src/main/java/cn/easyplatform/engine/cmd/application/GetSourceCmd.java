/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.application;

import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.entities.beans.task.DecisionBean;
import cn.easyplatform.entities.beans.task.TransitionBean;
import cn.easyplatform.entities.helper.EventLogic;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.GetSourceResponseMessage;
import cn.easyplatform.messages.vos.GetSourceVo;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.type.IResponseMessage;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetSourceCmd extends AbstractCommand<SimpleRequestMessage> {

	/**
	 * @param req
	 */
	public GetSourceCmd(SimpleRequestMessage req) {
		super(req);
	}

	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		String sid = (String) req.getBody();
		GetSourceVo gsv = new GetSourceVo();
		if (sid.compareTo("800") >= 0 && sid.compareTo("899") < 0) {
			WorkflowContext ctx = cc.getWorkflowContext();
			if (ctx == null)
				return new GetSourceResponseMessage(gsv);
			Object value = ctx.getParameter(sid);
			if (value instanceof EventLogic) {
				EventLogic el = (EventLogic) value;
				gsv.setType("epscript");
				if (!Strings.isBlank(el.getId())) {
					EntityInfo entity = cc.getEntityDao().getEntity(
							cc.getProjectService().getEntity().getEntityTableName(),
							el.getId());
					if (entity != null)
						gsv.setSource(entity.getContent());
					else
						gsv.setSource("");
				} else
					gsv.setSource(el.getContent());
			} else if (value instanceof DecisionBean) {
				DecisionBean db = (DecisionBean) value;
				StringBuilder sb = new StringBuilder();
				sb.append("<decision expr=\"").append(db.getExpr())
						.append("\">\n");
				for (TransitionBean tsb : db.getTransitions()) {
					sb.append("<transition name=\"").append(tsb.getName())
							.append("\" displayName=\"")
							.append(tsb.getDisplayName()).append("\" to=\"")
							.append(tsb.getTo()).append("\"");
					if (!Strings.isBlank(tsb.getFrom()))
						sb.append(" from=\"").append(tsb.getFrom())
								.append("\"");
					if (!Strings.isBlank(tsb.getLogicId()))
						sb.append(" logicId=\"").append(tsb.getLogicId())
								.append("\"");
					sb.append(">\n");
					if (!Strings.isBlank(tsb.getMappings()))
						sb.append(tsb.getMappings());
					sb.append("</transition>\n");
				}
				sb.append("</decision>");
				gsv.setSource(sb.toString());
			} else {
				EntityInfo entity = cc.getEntityDao().getEntity(
						cc.getProjectService().getEntity().getEntityTableName(),
						(String) value);
				if (entity != null)
					gsv.setSource(entity.getContent());
				else
					gsv.setSource("");
			}
		} else {
			EntityInfo entity = cc.getEntityDao().getEntity(
					cc.getProjectService().getEntity().getEntityTableName(), sid);
			if (entity != null) {
				if (entity.getType().equals(EntityType.LOGIC.getName()))
					gsv.setType("epscript");
				gsv.setSource(entity.getContent());
			} else
				gsv.setSource("");
		}
		return new GetSourceResponseMessage(gsv);
	}
}
