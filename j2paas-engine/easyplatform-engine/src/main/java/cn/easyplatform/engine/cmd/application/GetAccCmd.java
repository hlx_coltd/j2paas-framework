/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.application;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.GetAccRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.AccVo;
import cn.easyplatform.messages.vos.datalist.ListAccVo;
import cn.easyplatform.type.IResponseMessage;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetAccCmd extends AbstractCommand<GetAccRequestMessage> {

    /**
     * @param req
     */
    public GetAccCmd(GetAccRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        AccVo av = req.getBody();
        Integer dpt = cc.getAcc(av.getField());
        if (dpt != null)
            return new SimpleResponseMessage(dpt);
        if (av instanceof ListAccVo) {
            ListAccVo lv = (ListAccVo) av;
            if (lv.getKeys() == null)
                return new SimpleResponseMessage(cc.getAcc(lv.getField()));
            else {
                String name = lv.getField();
                if (name.length() == 3 && name.compareTo("700") >= 0
                        && name.compareTo("899") < 0) {
                    String acc = (String) cc.getWorkflowContext().getRecord()
                            .getValue(av.getField());
                    return new SimpleResponseMessage(cc.getAcc(acc));
                } else {
                    ListContext lc = cc.getWorkflowContext()
                            .getList(lv.getId());
                    if (lc != null) {
                        RecordContext rc = lc.getRecord(lv.getKeys());
                        if (rc != null) {
                            String acc = (String) rc.getValue(av.getField());
                            return new SimpleResponseMessage(cc.getAcc(acc));
                        }
                    }
                }
            }
            return new SimpleResponseMessage();
        } else {
            String acc = (String) cc.getWorkflowContext().getRecord()
                    .getValue(av.getField());
            return new SimpleResponseMessage(cc.getAcc(acc));
        }
    }

}
