/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.datalist;

import cn.easyplatform.type.ListRowVo;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListDropVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String filter;// 源列表和目标表唯一值表达式

	private String sourceId;

	private List<ListRowVo> data;

	private String targetId;

	private Object key;// 关键值，如果是列表，表示HOST的来源记录KEY,如果SWIFT，表示LinkVo集合

	/**
	 * @param sourceId
	 * @param data
	 * @param clear
	 */
	public ListDropVo(String filter, String sourceId,
			List<ListRowVo> data) {
		this.filter = filter;
		this.sourceId = sourceId;
		this.data = data;
	}

	/**
	 * @return the sourceId
	 */
	public String getSourceId() {
		return sourceId;
	}

	/**
	 * @return the data
	 */
	public List<ListRowVo> getData() {
		return data;
	}

	/**
	 * @return the targetId
	 */
	public String getTargetId() {
		return targetId;
	}

	/**
	 * @param targetId
	 *            the targetId to set
	 */
	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	/**
	 * @return the key
	 */
	public Object getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(Object key) {
		this.key = key;
	}

	public String getFilter() {
		return filter;
	}

}
