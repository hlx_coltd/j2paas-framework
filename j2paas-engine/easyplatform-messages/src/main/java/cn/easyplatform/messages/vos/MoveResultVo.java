/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MoveResultVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6583969356712471305L;

	private int index = -1;

	private Object data;

	private Object[] orginData;

	private boolean dropSource;
	
	/**
	 * @param data
	 */
	public MoveResultVo(Object[] orginData, Object data) {
		this.data = data;
		this.orginData = orginData;
	}

	/**
	 * @param index
	 * @param data
	 */
	public MoveResultVo(Object[] orginData, int index, Object data) {
		this.orginData = orginData;
		this.index = index;
		this.data = data;
	}

	public int getIndex() {
		return index;
	}

	public Object getData() {
		return data;
	}

	/**
	 * @return the orginData
	 */
	public Object[] getOrginData() {
		return orginData;
	}

	/**
	 * @return the dropSource
	 */
	public boolean isDropSource() {
		return dropSource;
	}

	/**
	 * @param dropSource the dropSource to set
	 */
	public void setDropSource(boolean dropSource) {
		this.dropSource = dropSource;
	}

}
