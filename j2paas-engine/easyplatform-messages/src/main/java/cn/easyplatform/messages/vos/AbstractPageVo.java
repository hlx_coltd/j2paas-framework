/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import java.io.Serializable;

import cn.easyplatform.type.Constants;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class AbstractPageVo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// 工作id
	private String id;
	// 图标
	private String image;
	// 标题
	private String titile;
	// 打开方式，参考OpenModel.class
	private int openModel = Constants.OPEN_NORMAL;
	//OSC方式下返回sessionId
	private Serializable sessionId;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the titile
	 */
	public String getTitile() {
		return titile;
	}

	/**
	 * @param titile
	 *            the titile to set
	 */
	public void setTitile(String titile) {
		this.titile = titile;
	}

	/**
	 * @return the openModel
	 */
	public int getOpenModel() {
		return openModel;
	}

	/**
	 * @param openModel the openModel to set
	 */
	public void setOpenModel(int openModel) {
		this.openModel = openModel;
	}

	public Serializable getSessionId() {
		return sessionId;
	}

	public void setSessionId(Serializable sessionId) {
		this.sessionId = sessionId;
	}
}
