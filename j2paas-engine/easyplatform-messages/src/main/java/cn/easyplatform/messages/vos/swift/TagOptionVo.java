/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.swift;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TagOptionVo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;

	private boolean moFlag;

	private int slashLine1;

	private int slashPos1;

	private int slashLine2;

	private int slashPos2;

	private int width1;

	private int height1;

	private int width2;

	private int height2;

	private String format;

	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * @param format the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the moFlag
	 */
	public boolean isMoFlag() {
		return moFlag;
	}

	/**
	 * @param moFlag
	 *            the moFlag to set
	 */
	public void setMoFlag(boolean moFlag) {
		this.moFlag = moFlag;
	}

	/**
	 * @return the slashLine1
	 */
	public int getSlashLine1() {
		return slashLine1;
	}

	/**
	 * @param slashLine1
	 *            the slashLine1 to set
	 */
	public void setSlashLine1(int slashLine1) {
		this.slashLine1 = slashLine1;
	}

	/**
	 * @return the slashPos1
	 */
	public int getSlashPos1() {
		return slashPos1;
	}

	/**
	 * @param slashPos1
	 *            the slashPos1 to set
	 */
	public void setSlashPos1(int slashPos1) {
		this.slashPos1 = slashPos1;
	}

	/**
	 * @return the slashLine2
	 */
	public int getSlashLine2() {
		return slashLine2;
	}

	/**
	 * @param slashLine2
	 *            the slashLine2 to set
	 */
	public void setSlashLine2(int slashLine2) {
		this.slashLine2 = slashLine2;
	}

	/**
	 * @return the slashPos2
	 */
	public int getSlashPos2() {
		return slashPos2;
	}

	/**
	 * @param slashPos2
	 *            the slashPos2 to set
	 */
	public void setSlashPos2(int slashPos2) {
		this.slashPos2 = slashPos2;
	}

	/**
	 * @return the width1
	 */
	public int getWidth1() {
		return width1;
	}

	/**
	 * @param width1
	 *            the width1 to set
	 */
	public void setWidth1(int width1) {
		this.width1 = width1;
	}

	/**
	 * @return the height1
	 */
	public int getHeight1() {
		return height1;
	}

	/**
	 * @param height1
	 *            the height1 to set
	 */
	public void setHeight1(int height1) {
		this.height1 = height1;
	}

	/**
	 * @return the width2
	 */
	public int getWidth2() {
		return width2;
	}

	/**
	 * @param width2
	 *            the width2 to set
	 */
	public void setWidth2(int width2) {
		this.width2 = width2;
	}

	/**
	 * @return the height2
	 */
	public int getHeight2() {
		return height2;
	}

	/**
	 * @param height2
	 *            the height2 to set
	 */
	public void setHeight2(int height2) {
		this.height2 = height2;
	}
}
