/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MoveVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -856727893617843811L;

	private int type;

	private boolean unique;

	private String sourceId;

	private String targetId;

	private List<Object[]> sourceData;

	private List<Object[]> targetData;

	private String filter;

	private String mapping;

	private String listboxQuery;//如果目标列表的记录是空的，新增时需要知道数据的类型
	
	public MoveVo(int type, boolean unique, String sourceId, String targetId,
			String filter, String mapping) {
		this.type = type;
		this.unique = unique;
		this.sourceId = sourceId;
		this.targetId = targetId;
		this.filter = filter;
		this.mapping = mapping;
	}

	public String getFilter() {
		return filter;
	}

	public String getMapping() {
		return mapping;
	}

	public String getSourceId() {
		return sourceId;
	}

	public String getTargetId() {
		return targetId;
	}

	public int getType() {
		return type;
	}

	public List<Object[]> getSourceData() {
		return sourceData;
	}

	public void setSourceData(List<Object[]> sourceData) {
		this.sourceData = sourceData;
	}

	public List<Object[]> getTargetData() {
		return targetData;
	}

	public void setTargetData(List<Object[]> targetData) {
		this.targetData = targetData;
	}

	public boolean isUnique() {
		return unique;
	}

	/**
	 * @return the listboxQuery
	 */
	public String getListboxQuery() {
		return listboxQuery;
	}

	/**
	 * @param listboxQuery the listboxQuery to set
	 */
	public void setListboxQuery(String listboxQuery) {
		this.listboxQuery = listboxQuery;
	}

}
