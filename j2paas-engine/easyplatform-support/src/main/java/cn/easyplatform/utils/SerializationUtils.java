/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.utils;

import org.apache.commons.io.IOUtils;

import java.io.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class SerializationUtils {

	public static byte[] serialize(Object object) {
		if (object == null) {
			return null;
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(object);
			oos.flush();
			return baos.toByteArray();
		} catch (IOException ex) {
			throw new IllegalArgumentException(
					"Failed to serialize object of type: " + object.getClass(),
					ex);
		} finally {
			IOUtils.closeQuietly(oos);
			IOUtils.closeQuietly(baos);
			baos = null;
		}
	}

	public static Object deserialize(byte[] bytes) {
		if (bytes == null) {
			return null;
		}
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new ByteArrayInputStream(bytes));
			return ois.readObject();
		} catch (IOException ex) {
			throw new IllegalArgumentException("Failed to deserialize object",
					ex);
		} catch (ClassNotFoundException ex) {
			throw new IllegalStateException(
					"Failed to deserialize object type", ex);
		} finally {
			IOUtils.closeQuietly(ois);
			ois = null;
		}
	}

}
