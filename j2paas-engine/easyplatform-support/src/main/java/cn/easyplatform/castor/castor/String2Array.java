/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.castor.castor;

import cn.easyplatform.castor.Castor;
import cn.easyplatform.castor.FailToCastObjectException;
import cn.easyplatform.lang.Lang;
import cn.easyplatform.lang.Strings;

import java.lang.reflect.Array;

public class String2Array extends Castor<String, Object> {

	public String2Array() {
		this.fromClass = String.class;
		this.toClass = Array.class;
	}

	@Override
	public Object cast(String src, Class<?> toType, String... args)
			throws FailToCastObjectException {
		if (Strings.isQuoteByIgnoreBlank(src, '[', ']')) {
			return "";//Json.fromJson(toType, src);
		}
		String[] ss = Strings.splitIgnoreBlank(src);
		return Lang.array2array(ss, toType.getComponentType());
	}

}
