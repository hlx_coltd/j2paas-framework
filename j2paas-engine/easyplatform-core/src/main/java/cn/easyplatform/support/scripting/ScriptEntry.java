/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.support.scripting;

import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.interceptor.CommandContext;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ScriptEntry {

	private CommandContext cc;

	private String script;

	private RecordContext[] rcs;

	private Object nameSpace;

	public ScriptEntry(CommandContext cc, String script, Object ns) {
		this.cc = cc;
		this.script = script;
		nameSpace = ns;
	}

	public ScriptEntry(CommandContext cc, String script, RecordContext[] rcs) {
		this.cc = cc;
		this.script = script;
		this.rcs = rcs;
	}

	public CommandContext getCommandContext() {
		return cc;
	}

	public void setCommandContext(CommandContext cc) {
		this.cc = cc;
	}

	public String getScript() {
		return script;
	}

	public RecordContext[] getContexts() {
		return rcs;
	}

	public Object getNameSpace() {
		return nameSpace;
	}
}
