/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.support.scripting;

import cn.easyplatform.entities.helper.EventLogic;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BreakPoint implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 中断的逻辑名称
	 */
	private EventLogic el;

	/**
	 * 解析器名字空间
	 */
	private Object ns;

	/**
	 * 中断的行数
	 */
	private int line;

	/**
	 * @param el
	 * @param ns
	 * @param line
	 */
	public BreakPoint(EventLogic el, Object ns, int line) {
		this.el = el;
		this.ns = ns;
		this.line = line;
	}

	/**
	 * @return
	 */
	public Object getNameSpace() {
		return ns;
	}

	/**
	 * @return
	 */
	public int getLine() {
		return line;
	}

	/**
	 * @return
	 */
	public EventLogic getEventLogic() {
		return el;
	}

	/**
	 * @param el
	 * @return
	 */
	public boolean isInstance(EventLogic el) {
		if (el.getId() != null && this.el.getId() != null) {
			if (el.getId().equals(this.el.getId()))
				return true;
		} else if (el.getContent() != null && this.el.getContent() != null) {
			if (el.getContent().equals(this.el.getContent()))
				return true;
		}
		return this.el == el;
	}
}
