/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.cfg.tx.jta;

import cn.easyplatform.EasyPlatformRuntimeException;
import cn.easyplatform.cfg.TransactionContext;

import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JtaTransactionContext implements TransactionContext {

	protected final TransactionManager transactionManager;

	public JtaTransactionContext(TransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	public void commit() {
		//忽略，由容器自动管理
	}

	public void rollback() {
		try {
			Transaction transaction = getTransaction();
			int status = transaction.getStatus();
			if (status != Status.STATUS_NO_TRANSACTION
					&& status != Status.STATUS_ROLLEDBACK) {
				transaction.setRollbackOnly();
			}
		} catch (IllegalStateException e) {
			throw new EasyPlatformRuntimeException(
					"Unexpected IllegalStateException while marking transaction rollback only");
		} catch (SystemException e) {
			throw new EasyPlatformRuntimeException(
					"SystemException while marking transaction rollback only");
		}
	}

	protected Transaction getTransaction() {
		try {
			return transactionManager.getTransaction();
		} catch (SystemException e) {
			throw new EasyPlatformRuntimeException(
					"SystemException while getting transaction ", e);
		}
	}

	@Override
	public void close() {
	}
}
