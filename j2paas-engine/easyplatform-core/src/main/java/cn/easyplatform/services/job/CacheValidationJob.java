/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.services.job;

import cn.easyplatform.cfg.EngineConfiguration;
import cn.easyplatform.contexts.Contexts;
import cn.easyplatform.contexts.EngineContext;
import cn.easyplatform.services.IProjectService;
import cn.easyplatform.spi.engine.EngineFactory;
import org.quartz.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class CacheValidationJob implements Job {

	//private final static Log log = Logs.getLog(PlatformJobWorker.class);

	//private final static int DEFAULT_TIMES = 30;// 30倍时间cacheValidationInterval

	//private final static String DEFAULT_COUNTER_NAME = "counter";

	@Override
	public void execute(JobExecutionContext ctx) throws JobExecutionException {
		try {
			EngineContext ec = (EngineContext) EngineFactory.me();
			EngineConfiguration engineConfiguration = ec.getEngineConfiguration();
			//刷新项目:包括参数、资源缓存的检查
			for (IProjectService ps : engineConfiguration.getProjectServices())
				ps.refresh("ENTITY");

/*			int count = Nums.toInt(
					ctx.getJobDetail().getJobDataMap()
							.get(DEFAULT_COUNTER_NAME), 0);
			if (count == DEFAULT_TIMES) {
				// 检查更新
				DataSource ds = (DataSource) engineConfiguration
						.getService(SystemServiceId.SYS_ENTITY_DS.getName());
				EntityDao dao = DaoFactory.createEntityDao(ds);
				boolean updateE = false, dropE = false;
				List<EntityInfo> entities = dao.getUpdatableModel();
				if (!entities.isEmpty()) {
					for (EntityInfo entity : entities) {
						BaseEntity bean = TransformerFactory.newInstance()
								.transformFromXml(entity);
						IService service = engineConfiguration
								.removeService(bean.getId());
						if (service != null)
							service.stopUserstop();
						if (entity.getStatus() == 'D')
							dropE = true;
						else
							updateE = true;
						if (bean instanceof ResourceBean) {
							if (!entity.getId().equals(
									SystemServiceId.SYS_ENTITY_DS.getName())) {
								if (log.isDebugEnabled())
									log.debugf(
											"update entity: Flag[%s] Id[%s],Name[%s]",
											entity.getStatus(), entity.getId(),
											entity.getName());
								if (updateE) {
									service = new DataSourceService(
											(ResourceBean) bean);
									engineConfiguration.addService(service);
									service.start();
								}
							}
						} else if (updateE) {
							service = new ProjectService((ProjectBean) bean);
							engineConfiguration.addService(service);
							service.start();
						}
					}
					dao.deleteAndUpdateModel(updateE, dropE);
				}
				ctx.getJobDetail().getJobDataMap().put(DEFAULT_COUNTER_NAME, 0);
			} else
				ctx.getJobDetail().getJobDataMap()
						.put(DEFAULT_COUNTER_NAME, ++count)
		} catch (DaoException ex) {
			if (log.isErrorEnabled())
				log.error(I18N.getLable(ex.getMessage(), ex.getArgs()), ex);
		} catch (Exception ex) {
			if (log.isErrorEnabled())
				log.error(ex);;*/
		} finally {
			Contexts.clear();
		}
	}

}
